/** ADC Library
	Definitions for calling IAP ROM functions
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "adc.h"

u32 adc_ctrl;

LPC_ADC_T *adc[] = {LPC_ADC0, LPC_ADC1};

/** Initialize the ADC peripheral */
void adc_init(void) {
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_ADC0_PD);
	SYSCTL_PowerUp(SYSCTL_POWERDOWN_ADC1_PD);
	ADC_SetIntEnable(LPC_ADC0,0);
	ADC_SetIntEnable(LPC_ADC1,0);
	ADC_SetTrim(LPC_ADC0,ADC_VRANGE);
	ADC_SetTrim(LPC_ADC1,ADC_VRANGE);
	adc_ctrl = ((SYS_CLK_ADC_FRQ/ADC_SAMPLE_FREQ/25-1)&0xff) | ((ADC_LOW_POWER_MODE != 0)?ADC_CTRL_LPWRMODE:0);
	ADC_SetCtrl(LPC_ADC0,adc_ctrl);
	ADC_SetCtrl(LPC_ADC1,adc_ctrl);
}

/** Shutdown ADC */
void adc_shutdown(void) {
	ADC_SetIntEnable(LPC_ADC0,0);
	ADC_SetIntEnable(LPC_ADC1,0);
	ADC_SetCtrl(LPC_ADC0,0);
	ADC_SetCtrl(LPC_ADC1,0);
	SYSCTL_PowerDown(SYSCTL_POWERDOWN_ADC0_PD);
	SYSCTL_PowerDown(SYSCTL_POWERDOWN_ADC1_PD);
}

/** Start ADC calibration */
void adc_start_calibration(void) {
	/* Set calibration mode */
	ADC_SetCtrl(LPC_ADC0,ADC_CTRL_CALMODE | ADC_CTRL_CLKDIV_FREQ(500000));
	ADC_SetCtrl(LPC_ADC1,ADC_CTRL_CALMODE | ADC_CTRL_CLKDIV_FREQ(500000));
}

/** Start sequence
    @param sequence index of sequence
    @param channels bitmask of channels to start
 */
void adc_start_conversion(u8 dev, u8 sequence, u32 channels) {
	u32 val = channels | ADC_SEQCTRL_SEQ_ENA;
	ADC_SetCtrl(adc[dev], adc_ctrl);
	ADC_SetSeqCtrl(adc[dev], sequence, val);
	ADC_SetSeqCtrl(adc[dev], sequence, val|ADC_SEQCTRL_START);
}
