/** Pin Access
	Configure pin function / direction / settings, read input pins, write output pins
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef PIN_H
#define PIN_H

#include "pin_cnf.h"

/*
Package		GPIO Port 0			GPIO Port 1			GPIO Port 2
LQFP48		PIO0_0 to PIO0_29	-					-
LQFP64		PIO0_0 to PIO0_31	PIO1_0 to PIO1_11	-
LQFP100		PIO0_0 to PIO0_31	PIO1_0 to PIO1_31	PIO2_0 to PIO2_11
 */


/* Pin access via channel identifiers */

/** Get port from channel identifier c */
#define PIN_CH_PORT(ch)   ( (u8)((ch) >> 5) )
/** Get pin from channel identifier c */
#define PIN_CH_PIN(ch)    ( (ch) & 0x1f )
/** Create channel identifier from port and pin */
#define PIN_CH(port,pin) (((port)<<5) | (pin) )


/** Set pin direction through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param level 0:lo or 1:hi
 */
#define PIN_ChSet(ch, value) {                                \
	if ((value)==0)                                           \
		LPC_GPIO->CLR[PIN_CH_PORT(ch)] = 1UL<<PIN_CH_PIN(ch); \
	else                                                      \
		LPC_GPIO->SET[PIN_CH_PORT(ch)] = 1UL<<PIN_CH_PIN(ch); \
}

/** Read pin input through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@return level 0:lo or 1:hi
 */
#define PIN_ChGet(ch) (((LPC_GPIO->PIN[PIN_CH_PORT(ch)] & (1UL<<PIN_CH_PIN(ch)) ) != 0)?1:0)

/** Set pin direction through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param dir 0 for input, 1 for output
 */
#define PIN_ChSetDir(ch, dir) {                                        \
	if ((dir)==0)                                                      \
		LPC_GPIO->DIR[PIN_CH_PORT(ch)] &= (u32)~(1UL<<PIN_CH_PIN(ch)); \
	else                                                               \
		LPC_GPIO->DIR[PIN_CH_PORT(ch)] |= (1UL<<PIN_CH_PIN(ch));       \
}

/** Set pin config through channel identifier.
	@param ch pin channel identifier (created via PIN_CH)
	@param cfg config (construct from PIN_IOCON_ macros)
 */
#define PIN_ChSetConfig(ch, cfg) {                                     \
		LPC_IOCON->PIO[PIN_CH_PORT(ch)][PIN_CH_PIN(ch)] = cfg;         \
}

/** Set Pin interrupt select register.
	@param x pin interrupt [0..7]
	@param y pin channel
 */
#define PIN_InterruptSelect(x, y) { LPC_INMUX->PINTSEL[x] = (y); }


/* Pin interrupt mode register */
#define PIN_ISEL_EDGE(x)  0                    ///! Edge sensitive pin interrupt x=0..7
#define PIN_ISEL_LEVEL(x) (1UL<<(x))           ///! Level sensitive pin interrupt x=0..7

/** Get Pin interrupt mode register.
	@return ISEL register]
 */
#define PIN_GetInterruptMode() ( LPC_GPIO_PIN_INT->ISEL)

/** Set Pin interrupt mode register.
	@param x value to write to ISEL register (use PIN_ISEL_xxx)]
 */
#define PIN_SetInterruptMode(x) { LPC_GPIO_PIN_INT->ISEL = (x); }

#define PIN_INT(x) (1UL<<(x))                  ///! Pin interrupt x=0..7

/** Enable rising edge or level interrupt.
	@param x mask of interrupt to enable (use PIN_INT_xxx)]
	@param en enable (0: disable, 1: enable)
 */
#define PIN_EnableRisingEdgeInt(x,en) { if (en==0) LPC_GPIO_PIN_INT->CIENR = (x); else LPC_GPIO_PIN_INT->SIENR = (x); }

/** Enable falling edge interrupt or set active level.
	@param x mask of interrupt to enable (use PIN_INT_xxx)]
	@param en enable (0: disable, 1: enable)
 */
#define PIN_EnableFallingEdgeInt(x,en) { if (en==0) LPC_GPIO_PIN_INT->CIENF = (x); else LPC_GPIO_PIN_INT->SIENF = (x); }

/** Get rising edge detection for pin interrupt.
	@return value of RISE register (set bits mean detected edges)
 */
#define PIN_GetRisingEdgeDetect(x) ( LPC_GPIO_PIN_INT->RISE )

/** Clear rising edge detection for pin interrupt.
    Writing a one clears the detection.
	@param x value to write to RISE register (use PIN_INT_xxx)]
 */
#define PIN_ClrRisingEdgeDetect(x) { LPC_GPIO_PIN_INT->RISE = (x); }

/** Get falling edge detection for pin interrupt.
	@return value of RISE register (set bits mean detected edges)
 */
#define PIN_GetFallingEdgeDetect(x) ( LPC_GPIO_PIN_INT->FALL )

/** Clear falling edge detection for pin interrupt.
    Writing a one clears the detection.
	@param x value to write to FALL register (use PIN_INT_xxx)]
 */
#define PIN_ClrFallingEdgeDetect(x) { LPC_GPIO_PIN_INT->FALL = (x); }


/** Get interrupt status.
	@return value of IST register (set bits mean interrupt requests)
 */
#define PIN_GetIntStatus(x) ( LPC_GPIO_PIN_INT->IST )

/** Clear edge interrupt request for pin interrupt.
    Writing a one clears the interrupt request in edge mode and toggles active edge in level mode.
	@param x value to write to FALL register (use PIN_INT_xxx)]
 */
#define PIN_ClrIntStatus(x) { LPC_GPIO_PIN_INT->IST = (x); }



/* Channel definitions */

#define PIN_0_0  PIN_CH(0, 0)
#define PIN_0_1  PIN_CH(0, 1)
#define PIN_0_2  PIN_CH(0, 2)
#define PIN_0_3  PIN_CH(0, 3)
#define PIN_0_4  PIN_CH(0, 4)
#define PIN_0_5  PIN_CH(0, 5)
#define PIN_0_6  PIN_CH(0, 6)
#define PIN_0_7  PIN_CH(0, 7)
#define PIN_0_8  PIN_CH(0, 8)
#define PIN_0_9  PIN_CH(0, 9)
#define PIN_0_10 PIN_CH(0,10)
#define PIN_0_11 PIN_CH(0,11)
#define PIN_0_12 PIN_CH(0,12)
#define PIN_0_13 PIN_CH(0,13)
#define PIN_0_14 PIN_CH(0,14)
#define PIN_0_15 PIN_CH(0,15)
#define PIN_0_16 PIN_CH(0,16)
#define PIN_0_17 PIN_CH(0,17)
#define PIN_0_18 PIN_CH(0,18)
#define PIN_0_19 PIN_CH(0,19)
#define PIN_0_20 PIN_CH(0,20)
#define PIN_0_21 PIN_CH(0,21)
#define PIN_0_22 PIN_CH(0,22)
#define PIN_0_23 PIN_CH(0,23)
#define PIN_0_24 PIN_CH(0,24)
#define PIN_0_25 PIN_CH(0,25)
#define PIN_0_26 PIN_CH(0,26)
#define PIN_0_27 PIN_CH(0,27)
#define PIN_0_28 PIN_CH(0,28)
#define PIN_0_29 PIN_CH(0,29)
#define PIN_0_30 PIN_CH(0,30)
#define PIN_0_31 PIN_CH(0,31)

#define PIN_1_0  PIN_CH(1, 0)
#define PIN_1_1  PIN_CH(1, 1)
#define PIN_1_2  PIN_CH(1, 2)
#define PIN_1_3  PIN_CH(1, 3)
#define PIN_1_4  PIN_CH(1, 4)
#define PIN_1_5  PIN_CH(1, 5)
#define PIN_1_6  PIN_CH(1, 6)
#define PIN_1_7  PIN_CH(1, 7)
#define PIN_1_8  PIN_CH(1, 8)
#define PIN_1_9  PIN_CH(1, 9)
#define PIN_1_10 PIN_CH(1,10)
#define PIN_1_11 PIN_CH(1,11)
#define PIN_1_12 PIN_CH(1,12)
#define PIN_1_13 PIN_CH(1,13)
#define PIN_1_14 PIN_CH(1,14)
#define PIN_1_15 PIN_CH(1,15)
#define PIN_1_16 PIN_CH(1,16)
#define PIN_1_17 PIN_CH(1,17)
#define PIN_1_18 PIN_CH(1,18)
#define PIN_1_19 PIN_CH(1,19)
#define PIN_1_20 PIN_CH(1,20)
#define PIN_1_21 PIN_CH(1,21)
#define PIN_1_22 PIN_CH(1,22)
#define PIN_1_23 PIN_CH(1,23)
#define PIN_1_24 PIN_CH(1,24)
#define PIN_1_25 PIN_CH(1,25)
#define PIN_1_26 PIN_CH(1,26)
#define PIN_1_27 PIN_CH(1,27)
#define PIN_1_28 PIN_CH(1,28)
#define PIN_1_29 PIN_CH(1,29)
#define PIN_1_31 PIN_CH(1,31)

#define PIN_2_0  PIN_CH(2, 0)
#define PIN_2_1  PIN_CH(2, 1)
#define PIN_2_2  PIN_CH(2, 2)
#define PIN_2_3  PIN_CH(2, 3)
#define PIN_2_4  PIN_CH(2, 4)
#define PIN_2_5  PIN_CH(2, 5)
#define PIN_2_6  PIN_CH(2, 6)
#define PIN_2_7  PIN_CH(2, 7)
#define PIN_2_8  PIN_CH(2, 8)
#define PIN_2_9  PIN_CH(2, 9)
#define PIN_2_10 PIN_CH(2,10)
#define PIN_2_11 PIN_CH(2,11)
#define PIN_2_12 PIN_CH(2,12)
#define PIN_2_13 PIN_CH(2,13)

#define PIN_NA 0xff


/* Port channel access macros */
#define PIN_PORT_0 0
#define PIN_PORT_1 1

/** Pin Initialization - configures all pins as specified in pin_cnf.h
 *  Needs to be changed for LQFP66 and LQFP100 (PIO[1][x] and PIO[2][x]).
 */
#define PIN_Init() {                                     \
		LPC_GPIO->PIN[0]         = PIN_PORT0_LVL_CFG;    \
		LPC_GPIO->PIN[1]         = PIN_PORT1_LVL_CFG;    \
		LPC_GPIO->PIN[2]         = PIN_PORT2_LVL_CFG;    \
		LPC_IOCON->PIO[0][0]     = PIN_IOCON_P0_0_CFG;   \
		LPC_IOCON->PIO[0][1]     = PIN_IOCON_P0_1_CFG;   \
		LPC_IOCON->PIO[0][2]     = PIN_IOCON_P0_2_CFG;   \
		LPC_IOCON->PIO[0][3]     = PIN_IOCON_P0_3_CFG;   \
		LPC_IOCON->PIO[0][4]     = PIN_IOCON_P0_4_CFG;   \
		LPC_IOCON->PIO[0][5]     = PIN_IOCON_P0_5_CFG;   \
		LPC_IOCON->PIO[0][6]     = PIN_IOCON_P0_6_CFG;   \
		LPC_IOCON->PIO[0][7]     = PIN_IOCON_P0_7_CFG;   \
		LPC_IOCON->PIO[0][8]     = PIN_IOCON_P0_8_CFG;   \
		LPC_IOCON->PIO[0][9]     = PIN_IOCON_P0_9_CFG;   \
		LPC_IOCON->PIO[0][10]    = PIN_IOCON_P0_10_CFG;  \
		LPC_IOCON->PIO[0][11]    = PIN_IOCON_P0_11_CFG;  \
		LPC_IOCON->PIO[0][12]    = PIN_IOCON_P0_12_CFG;  \
		LPC_IOCON->PIO[0][13]    = PIN_IOCON_P0_13_CFG;  \
		LPC_IOCON->PIO[0][14]    = PIN_IOCON_P0_14_CFG;  \
		LPC_IOCON->PIO[0][15]    = PIN_IOCON_P0_15_CFG;  \
		LPC_IOCON->PIO[0][16]    = PIN_IOCON_P0_16_CFG;  \
		LPC_IOCON->PIO[0][17]    = PIN_IOCON_P0_17_CFG;  \
		LPC_IOCON->PIO[0][18]    = PIN_IOCON_P0_18_CFG;  \
		LPC_IOCON->PIO[0][19]    = PIN_IOCON_P0_19_CFG;  \
		LPC_IOCON->PIO[0][20]    = PIN_IOCON_P0_20_CFG;  \
		LPC_IOCON->PIO[0][21]    = PIN_IOCON_P0_21_CFG;  \
		LPC_IOCON->PIO[0][22]    = PIN_IOCON_P0_22_CFG;  \
		LPC_IOCON->PIO[0][23]    = PIN_IOCON_P0_23_CFG;  \
		LPC_IOCON->PIO[0][24]    = PIN_IOCON_P0_24_CFG;  \
		LPC_IOCON->PIO[0][25]    = PIN_IOCON_P0_25_CFG;  \
		LPC_IOCON->PIO[0][26]    = PIN_IOCON_P0_26_CFG;  \
		LPC_IOCON->PIO[0][27]    = PIN_IOCON_P0_27_CFG;  \
		LPC_IOCON->PIO[0][28]    = PIN_IOCON_P0_28_CFG;  \
		LPC_IOCON->PIO[0][29]    = PIN_IOCON_P0_29_CFG;  \
		LPC_IOCON->PIO[0][30]    = PIN_IOCON_P0_30_CFG;  \
		LPC_IOCON->PIO[0][31]    = PIN_IOCON_P0_31_CFG;  \
		LPC_GPIO->DIR[0]         = PIN_PORT0_DIR_CFG;    \
		LPC_GPIO->DIR[1]         = PIN_PORT1_DIR_CFG;    \
		LPC_GPIO->DIR[2]         = PIN_PORT2_DIR_CFG;    \
		LPC_SWM->PINASSIGN[0]    = PIN_PINASSIGN0_CFG;   \
		LPC_SWM->PINASSIGN[1]    = PIN_PINASSIGN1_CFG;   \
		LPC_SWM->PINASSIGN[2]    = PIN_PINASSIGN2_CFG;   \
		LPC_SWM->PINASSIGN[3]    = PIN_PINASSIGN3_CFG;   \
		LPC_SWM->PINASSIGN[4]    = PIN_PINASSIGN4_CFG;   \
		LPC_SWM->PINASSIGN[5]    = PIN_PINASSIGN5_CFG;   \
		LPC_SWM->PINASSIGN[6]    = PIN_PINASSIGN6_CFG;   \
		LPC_SWM->PINASSIGN[7]    = PIN_PINASSIGN7_CFG;   \
		LPC_SWM->PINASSIGN[8]    = PIN_PINASSIGN8_CFG;   \
		LPC_SWM->PINASSIGN[9]    = PIN_PINASSIGN9_CFG;   \
		LPC_SWM->PINASSIGN[10]   = PIN_PINASSIGN10_CFG;  \
		LPC_SWM->PINASSIGN[11]   = PIN_PINASSIGN11_CFG;  \
		LPC_SWM->PINASSIGN[12]   = PIN_PINASSIGN12_CFG;  \
		LPC_SWM->PINASSIGN[13]   = PIN_PINASSIGN13_CFG;  \
		LPC_SWM->PINASSIGN[14]   = PIN_PINASSIGN14_CFG;  \
		LPC_SWM->PINASSIGN[15]   = PIN_PINASSIGN15_CFG;  \
		LPC_SWM->PINENABLE[0]    = PIN_PINENABLE0_CFG;   \
		LPC_SWM->PINENABLE[1]    = PIN_PINENABLE1_CFG;   \
}



/* Note: everything below this point is just to automatically create the macros for PIN_Init() */
#define PIN_PORT0_LVL_CFG (PIN_P0_0_LVL_CFG      |(PIN_P0_1_LVL_CFG<<1)  |(PIN_P0_2_LVL_CFG<<2)  |(PIN_P0_3_LVL_CFG<<3)  |(PIN_P0_4_LVL_CFG<<4)  |(PIN_P0_5_LVL_CFG<<5)  |(PIN_P0_6_LVL_CFG<<6)   | \
                          (PIN_P0_7_LVL_CFG<<7)  |(PIN_P0_8_LVL_CFG<<8)  |(PIN_P0_9_LVL_CFG<<9)  |(PIN_P0_10_LVL_CFG<<10)|(PIN_P0_11_LVL_CFG<<11)|(PIN_P0_12_LVL_CFG<<12)|(PIN_P0_13_LVL_CFG<<13) | \
                          (PIN_P0_14_LVL_CFG<<14)|(PIN_P0_15_LVL_CFG<<15)|(PIN_P0_16_LVL_CFG<<16)|(PIN_P0_17_LVL_CFG<<17)|(PIN_P0_18_LVL_CFG<<18)|(PIN_P0_19_LVL_CFG<<19)|(PIN_P0_20_LVL_CFG<<20) | \
                          (PIN_P0_21_LVL_CFG<<21)|(PIN_P0_22_LVL_CFG<<22)|(PIN_P0_23_LVL_CFG<<23)|(PIN_P0_24_LVL_CFG<<24)|(PIN_P0_25_LVL_CFG<<25)|(PIN_P0_26_LVL_CFG<<26)|(PIN_P0_27_LVL_CFG<<27) | \
                          (PIN_P0_28_LVL_CFG<<28)|(PIN_P0_29_LVL_CFG<<29)|(PIN_P0_30_LVL_CFG<<30)|(PIN_P0_31_LVL_CFG<<31))

#define PIN_PORT1_LVL_CFG (PIN_P1_0_LVL_CFG      |(PIN_P1_1_LVL_CFG<<1)  |(PIN_P1_2_LVL_CFG<<2)  |(PIN_P1_3_LVL_CFG<<3)  |(PIN_P1_4_LVL_CFG<<4)  |(PIN_P1_5_LVL_CFG<<5)  |(PIN_P1_6_LVL_CFG<<6)   | \
                          (PIN_P1_7_LVL_CFG<<7)  |(PIN_P1_8_LVL_CFG<<8)  |(PIN_P1_9_LVL_CFG<<9)  |(PIN_P1_10_LVL_CFG<<10)|(PIN_P1_11_LVL_CFG<<11)|(PIN_P1_12_LVL_CFG<<12)|(PIN_P1_13_LVL_CFG<<13) | \
                          (PIN_P1_14_LVL_CFG<<14)|(PIN_P1_15_LVL_CFG<<15)|(PIN_P1_16_LVL_CFG<<16)|(PIN_P1_17_LVL_CFG<<17)|(PIN_P1_18_LVL_CFG<<18)|(PIN_P1_19_LVL_CFG<<19)|(PIN_P1_20_LVL_CFG<<20) | \
                          (PIN_P1_21_LVL_CFG<<21)|(PIN_P1_22_LVL_CFG<<22)|(PIN_P1_23_LVL_CFG<<23)|(PIN_P1_24_LVL_CFG<<24)|(PIN_P1_25_LVL_CFG<<25)|(PIN_P1_26_LVL_CFG<<26)|(PIN_P1_27_LVL_CFG<<27) | \
                          (PIN_P1_28_LVL_CFG<<28)|(PIN_P1_29_LVL_CFG<<29)|(PIN_P1_30_LVL_CFG<<30)|(PIN_P1_31_LVL_CFG<<31))

#define PIN_PORT2_LVL_CFG (PIN_P2_0_LVL_CFG      |(PIN_P2_1_LVL_CFG<<1)  |(PIN_P2_2_LVL_CFG<<2)  |(PIN_P2_3_LVL_CFG<<3)  |(PIN_P2_4_LVL_CFG<<4)  |(PIN_P2_5_LVL_CFG<<5)  |(PIN_P2_6_LVL_CFG<<6)   | \
                          (PIN_P2_7_LVL_CFG<<7)  |(PIN_P2_8_LVL_CFG<<8)  |(PIN_P2_9_LVL_CFG<<9)  |(PIN_P2_10_LVL_CFG<<10)|(PIN_P2_11_LVL_CFG<<11))

#define PIN_PORT0_DIR_CFG (PIN_P0_0_DIR_CFG      |(PIN_P0_1_DIR_CFG<<1)  |(PIN_P0_2_DIR_CFG<<2)  |(PIN_P0_3_DIR_CFG<<3)  |(PIN_P0_4_DIR_CFG<<4)  |(PIN_P0_5_DIR_CFG<<5)  |(PIN_P0_6_DIR_CFG<<6)   | \
                          (PIN_P0_7_DIR_CFG<<7)  |(PIN_P0_8_DIR_CFG<<8)  |(PIN_P0_9_DIR_CFG<<9)  |(PIN_P0_10_DIR_CFG<<10)|(PIN_P0_11_DIR_CFG<<11)|(PIN_P0_12_DIR_CFG<<12)|(PIN_P0_13_DIR_CFG<<13) | \
                          (PIN_P0_14_DIR_CFG<<14)|(PIN_P0_15_DIR_CFG<<15)|(PIN_P0_16_DIR_CFG<<16)|(PIN_P0_17_DIR_CFG<<17)|(PIN_P0_18_DIR_CFG<<18)|(PIN_P0_19_DIR_CFG<<19)|(PIN_P0_20_DIR_CFG<<20) | \
                          (PIN_P0_21_DIR_CFG<<21)|(PIN_P0_22_DIR_CFG<<22)|(PIN_P0_23_DIR_CFG<<23)|(PIN_P0_24_DIR_CFG<<24)|(PIN_P0_25_DIR_CFG<<25)|(PIN_P0_26_DIR_CFG<<26)|(PIN_P0_27_DIR_CFG<<27) | \
                          (PIN_P0_28_DIR_CFG<<28)|(PIN_P0_29_DIR_CFG<<29)|(PIN_P0_30_DIR_CFG<<30)|(PIN_P0_31_DIR_CFG<<31))

#define PIN_PORT1_DIR_CFG (PIN_P1_0_DIR_CFG      |(PIN_P1_1_DIR_CFG<<1)  |(PIN_P1_2_DIR_CFG<<2)  |(PIN_P1_3_DIR_CFG<<3)  |(PIN_P1_4_DIR_CFG<<4)  |(PIN_P1_5_DIR_CFG<<5)  |(PIN_P1_6_DIR_CFG<<6)   | \
                          (PIN_P1_7_DIR_CFG<<7)  |(PIN_P1_8_DIR_CFG<<8)  |(PIN_P1_9_DIR_CFG<<9)  |(PIN_P1_10_DIR_CFG<<10)|(PIN_P1_11_DIR_CFG<<11)|(PIN_P1_12_DIR_CFG<<12)|(PIN_P1_13_DIR_CFG<<13) | \
                          (PIN_P1_14_DIR_CFG<<14)|(PIN_P1_15_DIR_CFG<<15)|(PIN_P1_16_DIR_CFG<<16)|(PIN_P1_17_DIR_CFG<<17)|(PIN_P1_18_DIR_CFG<<18)|(PIN_P1_19_DIR_CFG<<19)|(PIN_P1_20_DIR_CFG<<20) | \
                          (PIN_P1_21_DIR_CFG<<21)|(PIN_P1_22_DIR_CFG<<22)|(PIN_P1_23_DIR_CFG<<23)|(PIN_P1_24_DIR_CFG<<24)|(PIN_P1_25_DIR_CFG<<25)|(PIN_P1_26_DIR_CFG<<26)|(PIN_P1_27_DIR_CFG<<27) | \
                          (PIN_P1_28_DIR_CFG<<28)|(PIN_P1_29_DIR_CFG<<29)|(PIN_P1_30_DIR_CFG<<30)|(PIN_P1_31_DIR_CFG<<31))

#define PIN_PORT2_DIR_CFG (PIN_P2_0_DIR_CFG      |(PIN_P2_1_DIR_CFG<<1)  |(PIN_P2_2_DIR_CFG<<2)  |(PIN_P2_3_DIR_CFG<<3)  |(PIN_P2_4_DIR_CFG<<4)  |(PIN_P2_5_DIR_CFG<<5)  |(PIN_P2_6_DIR_CFG<<6)   | \
                          (PIN_P2_7_DIR_CFG<<7)  |(PIN_P2_8_DIR_CFG<<8)  |(PIN_P2_9_DIR_CFG<<9)  |(PIN_P2_10_DIR_CFG<<10)|(PIN_P2_11_DIR_CFG<<11))


/* Macros to access the configuration macros from pin_cnf.h */

#define PIN_IOCON_NOPULL       (0<<3)
#define PIN_IOCON_PULLDOWN     (1<<3)
#define PIN_IOCON_PULLUP       (2<<3)
#define PIN_IOCON_REPEATER     (3<<3)

#define PIN_IOCON_NO_HYS       (0<<5)
#define PIN_IOCON_HYS          (1<<5)

#define PIN_IOCON_NO_INV       (0<<6)
#define PIN_IOCON_INV          (1<<6)

#define PIN_IOCON_FILTR_ON     (0<<8)
#define PIN_IOCON_FILTR_OFF    (1<<8)

#define PIN_IOCON_I2C_FAST     (0<<8)
#define PIN_IOCON_I2C_STANDARD (1<<8)
#define PIN_IOCON_I2C_FASTPLUS (2<<8)

#define PIN_IOCON_NO_OD        (0<<10)
#define PIN_IOCON_OD           (1<<10)

#define PIN_FILTMODE_BYPASS    (0<<11)
#define PIN_FILTMODE_1CLK      (1<<11)
#define PIN_FILTMODE_2CLK      (2<<11)
#define PIN_FILTMODE_3CLK      (3<<11)

#define PIN_IOCON_CLKDIV1      (0<<13)
#define PIN_IOCON_CLKDIV2      (1<<13)
#define PIN_IOCON_CLKDIV4      (2<<13)
#define PIN_IOCON_CLKDIV8      (3<<13)
#define PIN_IOCON_CLKDIV16     (4<<13)
#define PIN_IOCON_CLKDIV32     (5<<13)
#define PIN_IOCON_CLKDIV64     (6<<13)

/* PINASSIGN */
#define PIN_PA0_UART0_TXD(x)        ((x)&0xff)
#define PIN_PA0_UART0_RXD(x)        (((x)&0xff)<<8)
#define PIN_PA0_UART0_RTS(x)        (((x)&0xff)<<16)
#define PIN_PA0_UART0_CTS(x)        (((x)&0xff)<<24)

#define PIN_PA1_UART0_SCLK(x)       ((x)&0xff)
#define PIN_PA1_UART1_TXD(x)        (((x)&0xff)<<8)
#define PIN_PA1_UART1_RXD(x)        (((x)&0xff)<<16)
#define PIN_PA1_UART1_RTS(x)        (((x)&0xff)<<24)

#define PIN_PA2_UART1_CTS(x)        ((x)&0xff)
#define PIN_PA2_UART1_SCLK(x)       (((x)&0xff)<<8)
#define PIN_PA2_UART2_TXD(x)        (((x)&0xff)<<16)
#define PIN_PA2_UART2_RXD(x)        (((x)&0xff)<<24)

#define PIN_PA3_UART2_SCLK(x)       ((x)&0xff)
#define PIN_PA3_SPI0_SCK(x)         (((x)&0xff)<<8)
#define PIN_PA3_SPI0_MOSI(x)        (((x)&0xff)<<16)
#define PIN_PA3_SPI0_MISO(x)        (((x)&0xff)<<24)

#define PIN_PA4_SPI0_SSELSN_0(x)    ((x)&0xff)
#define PIN_PA4_SPI0_SSELSN_1(x)    (((x)&0xff)<<8)
#define PIN_PA4_SPI0_SSELSN_2(x)    (((x)&0xff)<<16)
#define PIN_PA4_SPI0_SSELSN_3(x)    (((x)&0xff)<<24)

#define PIN_PA5_SPI1_SCK(x)         ((x)&0xff)
#define PIN_PA5_SPI1_MOSI(x)        (((x)&0xff)<<8)
#define PIN_PA5_SPI1_MISO(x)        (((x)&0xff)<<16)
#define PIN_PA5_SPI1_SSELSN_0(x)    (((x)&0xff)<<24)

#define PIN_PA6_SPI1_SSELSN_1(x)    ((x)&0xff)
#define PIN_PA6_CAN_TD1(x)          (((x)&0xff)<<8)
#define PIN_PA6_SPI1_CAN_RD1(x)     (((x)&0xff)<<16)
#define PIN_PA6_USB_CONNECT(x)      (((x)&0xff)<<24) /* missing in manual */

#define PIN_PA7_USB_VBUS(x)         ((x)&0xff)
#define PIN_PA7_SCT0_OUT0(x)        (((x)&0xff)<<8)
#define PIN_PA7_SCT0_OUT1(x)        (((x)&0xff)<<16)
#define PIN_PA7_SCT0_OUT2(x)        (((x)&0xff)<<24)

#define PIN_PA8_SCT1_OUT0(x)        ((x)&0xff)
#define PIN_PA8_SCT1_OUT1(x)        (((x)&0xff)<<8)
#define PIN_PA8_SCT1_OUT2(x)        (((x)&0xff)<<16)
#define PIN_PA8_SCT2_OUT0(x)        (((x)&0xff)<<24)

#define PIN_PA9_SCT2_OUT1(x)        ((x)&0xff)
#define PIN_PA9_SCT2_OUT2(x)        (((x)&0xff)<<8)
#define PIN_PA9_SCT3_OUT0(x)        (((x)&0xff)<<16)
#define PIN_PA9_SCT3_OUT1(x)        (((x)&0xff)<<24)

#define PIN_PA10_SCT3_OUT2(x)       ((x)&0xff)
#define PIN_PA10_SCT_ABORT0(x)      (((x)&0xff)<<8)
#define PIN_PA10_SCT_ABORT1(x)      (((x)&0xff)<<16)
#define PIN_PA10_ADC0_PIN_TRIG0(x)  (((x)&0xff)<<24)

#define PIN_PA11_ADC0_PIN_TRIG1(x)  ((x)&0xff)
#define PIN_PA11_ADC1_PIN_TRIG0(x)  (((x)&0xff)<<8)
#define PIN_PA11_ADC1_PIN_TRIG1(x)  (((x)&0xff)<<16)
#define PIN_PA11_DAC_PIN_TRIG(x)    (((x)&0xff)<<24)

#define PIN_PA12_DAC_SHUTOFF(x)     ((x)&0xff)
#define PIN_PA12_ACMP0_OUT(x)       (((x)&0xff)<<8)
#define PIN_PA12_ACMP1_OUT(x)       (((x)&0xff)<<16)
#define PIN_PA12_ACMP2_OUT(x)       (((x)&0xff)<<24)

#define PIN_PA13_ACMP3_OUT(x)       ((x)&0xff)
#define PIN_PA13_CLK_OUT(x)         (((x)&0xff)<<8)
#define PIN_PA13_ROSC0(x)           (((x)&0xff)<<16)
#define PIN_PA13_ROSC_RST0(x)       (((x)&0xff)<<24)

#define PIN_PA14_USB_FRAME_TOG(x)   ((x)&0xff)
#define PIN_PA14_QEI0_PHA(x)        (((x)&0xff)<<8)
#define PIN_PA14_QEI0_PHB(x)        (((x)&0xff)<<16)
#define PIN_PA14_QEI0_IDX(x)        (((x)&0xff)<<24)

#define PIN_PA15_GPIO_INT_BMATCH(x) ((x)&0xff)
#define PIN_PA15_SWO(x)             (((x)&0xff)<<8)

/* Pin function Enable */
#define PIN_PE0_ADC0_0(x)   ((x)<<0)
#define PIN_PE0_ADC0_1(x)   ((x)<<1)
#define PIN_PE0_ADC0_2(x)   ((x)<<2)
#define PIN_PE0_ADC0_3(x)   ((x)<<3)
#define PIN_PE0_ADC0_4(x)   ((x)<<4)
#define PIN_PE0_ADC0_5(x)   ((x)<<5)
#define PIN_PE0_ADC0_6(x)   ((x)<<6)
#define PIN_PE0_ADC0_7(x)   ((x)<<7)
#define PIN_PE0_ADC0_8(x)   ((x)<<8)
#define PIN_PE0_ADC0_9(x)   ((x)<<9)
#define PIN_PE0_ADC0_10(x)  ((x)<<10)
#define PIN_PE0_ADC0_11(x)  ((x)<<11)
#define PIN_PE0_ADC1_0(x)   ((x)<<12)
#define PIN_PE0_ADC1_1(x)   ((x)<<13)
#define PIN_PE0_ADC1_2(x)   ((x)<<14)
#define PIN_PE0_ADC1_3(x)   ((x)<<15)
#define PIN_PE0_ADC1_4(x)   ((x)<<16)
#define PIN_PE0_ADC1_5(x)   ((x)<<17)
#define PIN_PE0_ADC1_6(x)   ((x)<<18)
#define PIN_PE0_ADC1_7(x)   ((x)<<19)
#define PIN_PE0_ADC1_8(x)   ((x)<<20)
#define PIN_PE0_ADC1_9(x)   ((x)<<21)
#define PIN_PE0_ADC1_10(x)  ((x)<<22)
#define PIN_PE0_ADC1_11(x)  ((x)<<23)
#define PIN_PE0_DAC_OUT(x)  ((x)<<24)
#define PIN_PE0_ACMP_I1(x)  ((x)<<25)
#define PIN_PE0_ACMP_I2(x)  ((x)<<26)
#define PIN_PE0_ACMP0_I3(x) ((x)<<27)
#define PIN_PE0_ACMP0_I4(x) ((x)<<28)
#define PIN_PE0_ACMP1_I3(x) ((x)<<29)
#define PIN_PE0_ACMP1_I4(x) ((x)<<30)
#define PIN_PE0_ACMP2_I3(x) ((x)<<31)

#define PIN_PE1_ACMP2_I4(x)  ((x)<<0)
#define PIN_PE1_ACMP3_I3(x)  ((x)<<1)
#define PIN_PE1_ACMP3_I4(x)  ((x)<<2)
#define PIN_PE1_I2C0_SDA(x)  ((x)<<3)
#define PIN_PE1_I2C0_SCL(x)  ((x)<<4)
#define PIN_PE1_SCT0_OUT3(x) ((x)<<5)
#define PIN_PE1_SCT0_OUT4(x) ((x)<<6)
#define PIN_PE1_SCT0_OUT5(x) ((x)<<7)
#define PIN_PE1_SCT0_OUT6(x) ((x)<<8)
#define PIN_PE1_SCT0_OUT7(x) ((x)<<9)
#define PIN_PE1_SCT1_OUT3(x) ((x)<<10)
#define PIN_PE1_SCT1_OUT4(x) ((x)<<11)
#define PIN_PE1_SCT1_OUT5(x) ((x)<<12)
#define PIN_PE1_SCT1_OUT6(x) ((x)<<13)
#define PIN_PE1_SCT1_OUT7(x) ((x)<<14)
#define PIN_PE1_SCT2_OUT3(x) ((x)<<15)
#define PIN_PE1_SCT2_OUT4(x) ((x)<<16)
#define PIN_PE1_SCT2_OUT5(x) ((x)<<17)
#define PIN_PE1_SCT3_OUT3(x) ((x)<<18)
#define PIN_PE1_SCT3_OUT4(x) ((x)<<19)
#define PIN_PE1_SCT3_OUT5(x) ((x)<<20)
#define PIN_PE1_RESETN(x)    ((x)<<21)
#define PIN_PE1_SWCLK(x)     ((x)<<22)
#define PIN_PE1_SWDIO(x)     ((x)<<23)
#endif

