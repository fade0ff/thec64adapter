/*
 * @brief LPC15xx ROM API declarations and functions
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#ifndef __ROMAPI_15XX_H_
#define __ROMAPI_15XX_H_

/** @defgroup CHIP_I2CROM_15XX CHIP: LPC15xx I2C ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/**
 * @brief LPC15XX I2C ROM driver handle structure
 */
typedef void *I2C_HANDLE_T;

/**
 * @brief LPC15XX I2C ROM driver callback function
 */
typedef void  (*I2C_CALLBK_T)(uint32_t err_code, uint32_t n);

/**
 * LPC15XX I2C ROM driver parameter structure
 */
typedef struct I2C_PARAM {
	uint32_t        num_bytes_send;		///! No. of bytes to send
	uint32_t        num_bytes_rec;		///! No. of bytes to receive
	uint8_t         *buffer_ptr_send;	///! Pointer to send buffer
	uint8_t         *buffer_ptr_rec;	///! Pointer to receive buffer
	I2C_CALLBK_T    func_pt;			///! Callback function
	uint8_t         stop_flag;			///! Stop flag
	uint8_t         dummy[3];
} I2C_PARAM_T;

/**
 * LPC15XX I2C ROM driver result structure
 */
typedef struct I2C_RESULT {
	uint32_t n_bytes_sent;	///! No. of bytes sent
	uint32_t n_bytes_recd;	///! No. of bytes received
} I2C_RESULT_T;

/**
 * LPC15XX I2C ROM driver modes enum
 */
typedef enum CHIP_I2C_MODE {
	IDLE,			///! IDLE state
	MASTER_SEND,	///! Master send state
	MASTER_RECEIVE,	///! Master Receive state
	SLAVE_SEND,		///! Slave send state
	SLAVE_RECEIVE	///! Slave receive state
} CHIP_I2C_MODE_T;

/**
 * LPC15XX I2C ROM driver APIs structure
 */
typedef struct  I2CD_API {
	/// Interrupt Support Routine
	void (*i2c_isr_handler)(I2C_HANDLE_T *handle);
	// MASTER functions
	ErrorCode_t (*i2c_master_transmit_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_master_receive_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_master_tx_rx_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_master_transmit_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_master_receive_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_master_tx_rx_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);

	// SLAVE functions
	ErrorCode_t (*i2c_slave_receive_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_slave_transmit_poll)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_slave_receive_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_slave_transmit_intr)(I2C_HANDLE_T *handle, I2C_PARAM_T *param, I2C_RESULT_T *result);
	ErrorCode_t (*i2c_set_slave_addr)(I2C_HANDLE_T *handle, uint32_t slave_addr_0_3, uint32_t slave_mask_0_3);

	// OTHER support functions
	uint32_t (*i2c_get_mem_size)(void);
	I2C_HANDLE_T *(*i2c_setup)(uint32_t  i2c_base_addr, uint32_t * start_of_ram);
	ErrorCode_t (*i2c_set_bitrate)(I2C_HANDLE_T *handle, uint32_t  p_clk_in_hz, uint32_t bitrate_in_bps);
	uint32_t (*i2c_get_firmware_version)(void);
	CHIP_I2C_MODE_T (*i2c_get_status)(I2C_HANDLE_T *handle);
	ErrorCode_t (*i2c_set_timeout)(I2C_HANDLE_T *handle, uint32_t timeout);
} I2CD_API_T;

/**
 * @}
 */
 
/** @defgroup PWRROM_15XX CHIP: LPC15xx Power ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/**
 * @brief LPC15XX Power ROM APIs - set_pll mode options
 */
#define CPU_FREQ_EQU    0
#define CPU_FREQ_LTE    1
#define CPU_FREQ_GTE    2
#define CPU_FREQ_APPROX 3

/**
 * @brief LPC15XX Power ROM APIs - set_pll response0 options
 */
#define PLL_CMD_SUCCESS    0
#define PLL_INVALID_FREQ   1
#define PLL_INVALID_MODE   2
#define PLL_FREQ_NOT_FOUND 3
#define PLL_NOT_LOCKED     4

/**
 * @brief LPC15XX Power ROM APIs - set_power mode options
 */
#define PWR_DEFAULT         0
#define PWR_CPU_PERFORMANCE 1
#define PWR_EFFICIENCY      2
#define PWR_LOW_CURRENT     3

/**
 * @brief LPC15XX Power ROM APIs - set_power response0 options
 */
#define PWR_CMD_SUCCESS  0
#define PWR_INVALID_FREQ 1
#define PWR_INVALID_MODE 2

/**
 * @brief LPC15XX Power ROM APIs - power_mode_configure mode options
 */
#define PMU_SLEEP           0
#define PMU_DEEP_SLEEP      1
#define PMU_POWERDOWN       2
#define PMU_DEEP_POWERDOWN  3

/**
 * @brief LPC15XX Power ROM APIs - power_mode_configure peripheral control bits
 */
#define PMU_PD_WDOSC         (1 << 0)
#define PMU_PD_BOD           (1 << 1)
#define PMU_PD_ACMP0         (1 << 2)
#define PMU_PD_ACMP1         (1 << 3)
#define PMU_PD_ACMP2         (1 << 4)
#define PMU_PD_ACMP3         (1 << 5)
#define PMU_PD_IREF          (1 << 6)
#define PMU_PD_TS            (1 << 7)

/**
 * @brief LPC15xx Power ROM API structure
 * The power profile API provides functions to configure the system clock and optimize the
 * system setting for lowest power consumption.
 */
typedef struct PWRD_API {
	void (*set_pll)(uint32_t cmd[], uint32_t resp[]);	///! Set PLL function
	void (*set_power)(uint32_t cmd[], uint32_t resp[]);	///! Set power function
	void (*power_mode_configure)(uint32_t power_mode, uint32_t peripheral_ctrl);///! Sets the chip is low power modes
	void (*set_aclkgate)(uint32_t aclkgate);
	uint32_t (*get_aclkgate)(void);
} PWRD_API_T;

/**
 * @}
 */
 
/** @defgroup UARTROM_15XX CHIP: LPC15xx UART ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/**
 * @brief UART ROM driver - UART errors in UART configuration used in uart_init function
 */
#define OVERRUN_ERR_EN      (1 << 0)	///! Bit 0: Enable overrun error
#define UNDERRUN_ERR_EN     (1 << 1)	///! Bit 1: Enable underrun error
#define FRAME_ERR_EN        (1 << 2)	///! Bit 2: enable frame error
#define PARITY_ERR_EN       (1 << 3)	///! Bit 3: enable parity error
#define RXNOISE_ERR_EN      (1 << 4)	///! Bit 4: enable receive noise error

/**
 * Macros for UART errors
 */
/// Enable all the UART errors
#define ALL_ERR_EN          (OVERRUN_ERR_EN | UNDERRUN_ERR_EN | FRAME_ERR_EN | PARITY_ERR_EN | \
							 RXNOISE_ERR_EN)
/// Disable all the errors
#define NO_ERR_EN           (0)

/**
 * Transfer mode values in UART parameter structure.
 * Used in uart_get_line & uart_put_line function
 */
/// 0x00: uart_get_line: stop transfer when the buffer is full
/// 0x00: uart_put_line: stop transfer when the buffer is empty
#define TX_MODE_BUF_EMPTY       (0x00)
#define RX_MODE_BUF_FULL        (0x00)
/// 0x01: uart_get_line: stop transfer when CRLF are received
/// 0x01: uart_put_line: transfer stopped after reaching \0 and CRLF is sent out after that
#define TX_MODE_SZERO_SEND_CRLF (0x01)
#define RX_MODE_CRLF_RECVD      (0x01)
/// 0x02: uart_get_line: stop transfer when LF are received
/// 0x02: uart_put_line: transfer stopped after reaching \0. And LF is sent out after that
#define TX_MODE_SZERO_SEND_LF   (0x02)
#define RX_MODE_LF_RECVD        (0x02)
/// 0x03: uart_get_line: RESERVED
/// 0x03: uart_put_line: transfer stopped after reaching \0
#define TX_MODE_SZERO           (0x03)

/**
 * @brief UART ROM driver modes
 */
#define DRIVER_MODE_POLLING     (0x00)	///! Polling mode
#define DRIVER_MODE_INTERRUPT   (0x01)	///! Interrupt mode
#define DRIVER_MODE_DMA         (0x02)	///! DMA mode

/**
 * @brief UART ROM driver UART handle
 */
typedef void *UART_HANDLE_T;

/**
 * @brief UART ROM driver UART callback function
 */
typedef void (*UART_CALLBK_T)(uint32_t err_code, uint32_t n);

/**
 * @brief UART ROM driver UART DMA callback function
 */
typedef void (*UART_DMA_REQ_T)(uint32_t src_adr, uint32_t dst_adr, uint32_t size);

/**
 * @brief UART ROM driver configutaion structure
 */
typedef struct {
	uint32_t sys_clk_in_hz;		///! System clock in Hz
	uint32_t baudrate_in_hz;	///! Baud rate in Hz
	uint8_t  config;			/**< Configuration value
								      bit1:0  Data Length: 00: 7 bits length, 01: 8 bits length, others: reserved
								      bit3:2  Parity: 00: No Parity, 01: reserved, 10: Even, 11: Odd
								      bit4:   Stop Bit(s): 0: 1 Stop bit, 1: 2 Stop bits */
	uint8_t sync_mod;			/**< Sync mode settings
								      bit0:  Mode: 0: Asynchronous mode, 1: Synchronous  mode
								      bit1:  0: Un_RXD is sampled on the falling edge of SCLK
								             1: Un_RXD is sampled on the rising edge of SCLK
								      bit2:  0: Start and stop bits are transmitted as in asynchronous mode)
								             1: Start and stop bits are not transmitted)
								      bit3:  0: The UART is a  slave in Synchronous mode
								             1: The UART is a master in Synchronous mode */
	uint16_t error_en;			/**< Errors to be enabled
								      bit0: Overrun Errors Enabled
								      bit1: Underrun Errors Enabled
								      bit2: FrameErr Errors Enabled
								      bit3: ParityErr Errors Enabled
								      bit4: RxNoise Errors Enabled */
} UART_CONFIG_T;

/**
 * @brief UART ROM driver parameter structure
 */
typedef struct {
	uint8_t         *buffer;		///! Pointer to data buffer
	uint32_t        size;			///! Size of the buffer
	uint16_t        transfer_mode;	/**< Transfer mode settings
									       0x00: uart_get_line: stop transfer when the buffer is full
									       0x00: uart_put_line: stop transfer when the buffer is empty
									       0x01: uart_get_line: stop transfer when CRLF are received
									       0x01: uart_put_line: transfer stopped after reaching \0 and CRLF is sent out after that
									       0x02: uart_get_line: stop transfer when LF are received
									       0x02: uart_put_line: transfer stopped after reaching \0 and LF is sent out after that
									       0x03: uart_get_line: RESERVED
									       0x03: uart_put_line: transfer stopped after reaching \0 */
	uint8_t         driver_mode;	/**< Driver mode
									      0x00: Polling mode, function blocked until transfer completes
									      0x01: Interrupt mode, function immediately returns, callback invoked when transfer completes
									      0x02: DMA mode, in case DMA block is available, DMA req function is called for UART DMA channel setup, then callback function indicate that transfer completes */
	uint8_t         dma_num;		///! DMA channel number in case DMA mode is enabled
	UART_CALLBK_T   callback_func_pt;
	uint32_t dma;	/* DMA handler */
} UART_PARAM_T;

/**
 * @brief UART ROM driver APIs structure
 */
typedef struct UART_API {
	/* UART Configuration functions */
	uint32_t (*uart_get_mem_size)(void);	///! Get the memory size needed by one Min UART instance
	UART_HANDLE_T (*uart_setup)(uint32_t base_addr, uint8_t *ram);	///! Setup Min UART instance with provided memory and return the handle to this instance
	uint32_t (*uart_init)(UART_HANDLE_T handle, UART_CONFIG_T *set);	///! Setup baud rate and operation mode for uart, then enable uart

	/* UART polling functions block until completed */
	uint8_t (*uart_get_char)(UART_HANDLE_T handle);	///! Receive one Char from uart. This functions is only returned after Char is received. In case Echo is enabled, the received data is sent out immediately
	void (*uart_put_char)(UART_HANDLE_T handle, uint8_t data);	///! Send one Char through uart. This function is only returned after data is sent
	uint32_t (*uart_get_line)(UART_HANDLE_T handle, UART_PARAM_T *param);	///! Receive multiple bytes from UART
	uint32_t (*uart_put_line)(UART_HANDLE_T handle, UART_PARAM_T *param);	///! Send string (end with \0) or raw data through UART

	/* UART interrupt functions return immediately and callback when completed */
	void (*uart_isr)(UART_HANDLE_T handle);	///! UART interrupt service routine. To use this routine, the corresponding USART interrupt must be enabled. This function is invoked by the user ISR
} UARTD_API_T;

/**
 * @}
 */ 


#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup ROMAPI_15XX CHIP: LPC15xx ROM API declarations and functions
 * @ingroup CHIP_15XX_Drivers
 * @{
 */

/**
 * @brief LPC15XX High level ROM API structure
 */
typedef struct {
	const uint32_t pUSBD;					///! USBD API function table base address
	const uint32_t reserved0;				///! Reserved
	const CAND_API_T *pCAND;				///! C_CAN API function table base address
	const PWRD_API_T *pPWRD;				///! Power API function table base address
	const uint32_t reserved1;				///! Reserved
	const I2CD_API_T *pI2CD;				///! I2C driver API function table base address
	const DMAD_API_T *pDMAD;				///! DMA driver API function table base address
	const SPID_API_T *pSPID;				///! I2C driver API function table base address
	const ADCD_API_T *pADCD;				///! ADC driver API function table base address
	const UARTD_API_T *pUARTD;				///! UART driver API function table base address
} LPC_ROM_API_T;

/* Pointer to ROM API function address */
#define LPC_ROM_API_BASE_LOC    0x03000200UL
#define LPC_ROM_API     (*(LPC_ROM_API_T * *) LPC_ROM_API_BASE_LOC)

/* Pointer to @ref CAND_API_T functions in ROM */
#define LPC_CAND_API    ((LPC_ROM_API)->pCAND)

/* Pointer to @ref PWRD_API_T functions in ROM */
#define LPC_PWRD_API    ((LPC_ROM_API)->pPWRD)

/* Pointer to @ref I2CD_API_T functions in ROM */
#define LPC_I2CD_API    ((LPC_ROM_API)->pI2CD)

/* Pointer to @ref DMAD_API_T functions in ROM for DMA */
#define LPC_DMAD_API    ((LPC_ROM_API)->pDMAD)

/* Pointer to @ref SPID_API_T functions in ROM for DMA */
#define LPC_SPID_API    ((LPC_ROM_API)->pSPID)

/* Pointer to @ref ADCD_API_T functions in ROM for pADCD */
#define LPC_ADCD_API    ((LPC_ROM_API)->pADCD)

/* Pointer to @ref UARTD_API_T functions in ROM for UARTs */
#define LPC_UARTD_API   ((LPC_ROM_API)->pUARTD)

/* Pointer to ROM IAP entry functions */
#define IAP_ENTRY_LOCATION        0x03000205UL

/**
 * @brief LPC15XX IAP_ENTRY API function type
 */
static INLINE void iap_entry(unsigned int cmd_param[5], unsigned int status_result[4])
{
	((IAP_ENTRY_T) IAP_ENTRY_LOCATION)(cmd_param, status_result);
}

/**
 * @}
 */

/** @defgroup CANROM_15XX CHIP: LPC15xx CAN ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/* error status bits */
#define CAN_ERROR_NONE 0x00000000UL
#define CAN_ERROR_PASS 0x00000001UL
#define CAN_ERROR_WARN 0x00000002UL
#define CAN_ERROR_BOFF 0x00000004UL
#define CAN_ERROR_STUF 0x00000008UL
#define CAN_ERROR_FORM 0x00000010UL
#define CAN_ERROR_ACK 0x00000020UL
#define CAN_ERROR_BIT1 0x00000040UL
#define CAN_ERROR_BIT0 0x00000080UL
#define CAN_ERROR_CRC 0x00000100UL

typedef void *CAN_HANDLE_T;		/* define TYPE for CAN handle pointer */

typedef struct _CAN_MSG_OBJ {
	uint32_t mode_id;
	uint32_t mask;
	uint8_t data[8];
	uint8_t dlc;
	uint8_t msgobj;
} CAN_MSG_OBJ;

typedef struct _CAN_CALLBACKS {
	void (*CAN_rx)(uint8_t msg_obj);
	void (*CAN_tx)(uint8_t msg_obj);
	void (*CAN_error)(uint32_t error_info);
} CAN_CALLBACKS;

typedef struct _CAN_CFG {
	uint32_t clkdiv;
	uint32_t btr;
	uint32_t isr_ena;
} CAN_CFG;

typedef struct _CAN_ODCONSTENTRY {
	uint16_t index;
	uint8_t subindex;
	uint8_t len;
	uint32_t val;
} CAN_ODCONSTENTRY;

typedef struct _CAN_ODENTRY {
	uint16_t index;
	uint8_t subindex;
	uint8_t entrytype_len;
	uint8_t *val;
} CAN_ODENTRY;

typedef struct _CAN_CANOPENCFG {
	uint8_t node_id;
	uint8_t msgobj_rx;
	uint8_t msgobj_tx;
	uint8_t isr_handled;
	uint32_t od_const_num;
	CAN_ODCONSTENTRY *od_const_table;
	uint32_t od_num;
	CAN_ODENTRY *od_table;
} CAN_CANOPENCFG;

typedef struct _CANOPEN_CALLBACKS {
	uint32_t (*CANOPEN_sdo_read)(uint16_t index, uint8_t subindex);
	uint32_t (*CANOPEN_sdo_write)(uint16_t index, uint8_t subindex, uint8_t *dat_ptr);
	uint32_t (*CANOPEN_sdo_seg_read)(uint16_t index, uint8_t subindex, uint8_t
									 openclose, uint8_t *length, uint8_t *data, uint8_t *last);
	uint32_t (*CANOPEN_sdo_seg_write)(uint16_t index, uint8_t subindex, uint8_t
									  openclose, uint8_t length, uint8_t *data, uint8_t *fast_resp);
	uint8_t (*CANOPEN_sdo_req)(uint8_t length_req, uint8_t *req_ptr, uint8_t
							   *length_resp, uint8_t *resp_ptr);
} CANOPEN_CALLBACKS;

typedef struct _CAN_API_INIT_PARAM_T {
	uint32_t mem_base;			/* Address of user-space memory area to use */
	uint32_t can_reg_base;		/* Address of start of CAN controller register area */
	CAN_CFG *can_cfg;
	CAN_CALLBACKS *callbacks;
	CAN_CANOPENCFG *canopen_cfg;
	CANOPEN_CALLBACKS *co_callbacks;
} CAN_API_INIT_PARAM_T;

/**
 * @brief LPC15XX CAN ROM API structure
 * The CAN profile API provides functions to configure and manage the CAN sub-system.
 */
typedef struct _CAND_API_T {
	uint32_t (*hwCAN_GetMemSize)(CAN_API_INIT_PARAM_T *param);
	ErrorCode_t (*hwCAN_Init)(CAN_HANDLE_T *phCan, CAN_API_INIT_PARAM_T *param);
	void (*hwCAN_Isr)(CAN_HANDLE_T hCan);
	void (*hwCAN_ConfigRxmsgobj)(CAN_HANDLE_T hCan, CAN_MSG_OBJ *msg_obj);
	uint8_t (*hwCAN_MsgReceive)(CAN_HANDLE_T hCan, CAN_MSG_OBJ *msg_obj);
	void (*hwCAN_MsgTransmit)(CAN_HANDLE_T hCan, CAN_MSG_OBJ *msg_obj);
	void (*hwCAN_CANopenHandler)(CAN_HANDLE_T hCan);
} CAND_API_T;

uint32_t hwCAN_GetMemSize(CAN_API_INIT_PARAM_T *param);

ErrorCode_t hwCAN_Init(CAN_HANDLE_T *phCan, CAN_API_INIT_PARAM_T *param);

void hwCAN_Isr(CAN_HANDLE_T hCan);

void hwCAN_ConfigRxmsgobj(CAN_HANDLE_T hCan, CAN_MSG_OBJ *msg_obj);

uint8_t hwCAN_MsgReceive(CAN_HANDLE_T hCan, CAN_MSG_OBJ *msg_obj);

void hwCAN_MsgTransmit(CAN_HANDLE_T hCan, CAN_MSG_OBJ *msg_obj);

void hwCAN_CANopenHandler(CAN_HANDLE_T hCan);

/**
 * @}
 */
 
/** @defgroup DMAROM_15XX CHIP: LPC15xx DMA ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/* Bit definitions for DMA ROM Channel Configuration Structure */
#define DMA_ROM_CH_EVENT_SWTRIG                         ((uint8_t) 0)
#define DMA_ROM_CH_EVENT_PERIPH                         ((uint8_t) 1)
#define DMA_ROM_CH_EVENT_HWTRIG                         ((uint8_t) 2)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_1      ((uint8_t) 0 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_2      ((uint8_t) 1 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_4      ((uint8_t) 2 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_8      ((uint8_t) 3 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_16     ((uint8_t) 4 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_32     ((uint8_t) 5 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_64     ((uint8_t) 6 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_128    ((uint8_t) 7 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_256    ((uint8_t) 8 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_512    ((uint8_t) 9 << 0)
#define DMA_ROM_CH_HWTRIG_BURSTPOWER_1024   ((uint8_t) 10 << 0)
#define DMA_ROM_CH_HWTRIG_SRC_WRAP_EN               ((uint8_t) 1 << 4)
#define DMA_ROM_CH_HWTRIG_DEST_WRAP_EN          ((uint8_t) 1 << 5)
#define DMA_ROM_CH_HWTRIG_BURST_EN                  ((uint8_t) 1 << 6)
/* Bit definitions for DMA ROM Task Configuration Structure */
#define DMA_ROM_TASK_CFG_PING_PONG_EN               ((uint8_t) 1 << 0)
#define DMA_ROM_TASK_CFG_SW_TRIGGER                 ((uint8_t) 1 << 1)
#define DMA_ROM_TASK_CFG_CLR_TRIGGER                ((uint8_t) 1 << 2)
#define DMA_ROM_TASK_CFG_SEL_INTA                       ((uint8_t) 1 << 3)
#define DMA_ROM_TASK_CFG_SEL_INTB                       ((uint8_t) 1 << 4)
#define DMA_ROM_TASK_DATA_WIDTH_8                       ((uint8_t) 0 )
#define DMA_ROM_TASK_DATA_WIDTH_16                  ((uint8_t) 1 )
#define DMA_ROM_TASK_DATA_WIDTH_32                  ((uint8_t) 2 )
#define DMA_ROM_TASK_SRC_INC_0                          ((uint8_t) 0 << 2)
#define DMA_ROM_TASK_SRC_INC_1                          ((uint8_t) 1 << 2)
#define DMA_ROM_TASK_SRC_INC_2                          ((uint8_t) 2 << 2)
#define DMA_ROM_TASK_SRC_INC_4                          ((uint8_t) 3 << 2)
#define DMA_ROM_TASK_DEST_INC_0                         ((uint8_t) 0 << 4)
#define DMA_ROM_TASK_DEST_INC_1                         ((uint8_t) 1 << 4)
#define DMA_ROM_TASK_DEST_INC_2                         ((uint8_t) 2 << 4)
#define DMA_ROM_TASK_DEST_INC_4                         ((uint8_t) 3 << 4)
/**
 * @brief DMA handle type
 */
typedef void *DMA_HANDLE_T;

/**
 * @brief DMA channel callback function type
 * @param	res0: error code
 * @param	res1: 0 = INTA is issued, 1 = INTB is issued
 */
typedef void (*CALLBK_T)(uint32_t res0, uint32_t res1);

/**
 * @brief DMA ROM drivers channel control structure
 */
typedef struct {
	uint8_t event;		/**< event type selection for DMA transfer
						   - 0: software request
						   - 1: peripheral request
						   - 2: hardware trigger
						   - others: reserved */
	uint8_t hd_trigger;	/**< In case hardware trigger is enabled, the trigger burst is setup here.
						   NOTE: Rising edge triggered is fixed
						   - bit0~bit3: burst size
						    - 0: burst size =1, 1: 2^1, 2: 2^2,... 10: 1024, others: reserved.
						   - bit4: Source Burst Wrap
						    - 0: Source burst wrapping is not enabled
						    - 1: Source burst wrapping is enabled
						   - bit5: Destination Burst Wrap
						    - 0: Destination burst wrapping is not enabled
						    - 1: Destination burst wrapping is enabled
						   - bit6: Trigger Burst
						    - 0: Hardware trigger cause a single transfer
						    - 1: Hardware trigger cause a burst transfer
						   - bit7: reserved */
	uint8_t priority;	/**< priority level
						   - 0 -> 7: Highest priority ->  Lowest priority.
						   - other: reserved. */
	uint8_t reserved0;
	CALLBK_T cb_func;	/**< callback function, Callback function is
						            only invoked when INTA or INTB is enabled. */
}  DMA_CHANNEL_T;

/**
 * @brief DMA ROM driver's TASK parameter structure
 */
typedef struct {
	uint8_t ch_num;		///! DMA channel number
	uint8_t config;		/**< configuration of this task
						   - bit0: Ping_Pong transfer
						    - 0: Not Ping_Pong transfer
						    - 1: Linked with previous task for Ping_Pong transfer
						   - bit1: Software Trigger.
						    - 0: the trigger for this channel is not set.
						    - 1: the trigger for this channel is set immediately.
						   - bit2:  Clear Trigger
						    - 0: The trigger is not cleared when this task is finished.
						    - 1: The trigger is cleared when this task is finished.
						   - bit3:  Select INTA
						    - 0: No IntA.
						    - 1: The IntB flag for this channel will be set when this task is finished.
						   bit4:  Select INTB
						    0: No IntB.
						    1: The IntB flag for this channel will be set when this task is finished.
						   bit5~bit7: reserved
						 */

	uint8_t data_type;	/**<
						    - bit0~bit1: Data width. 0: 8-bit, 1: 16-bit, 2: 32-bit, 3: reserved
						    - bit2~bit3: How is source address incremented?
						        - 0: The source address is not incremented for each transfer.
						        1: The source address is incremented by the amount specified by Width for each transfer.
						        2: The source address is incremented by 2 times the amount specified by Width for each transfer.
						        3: The source address is incremented by 4 times the amount specified by Width for each transfer.
						    - bit4~bit5: How is the destination address incremented?
						        0: The destination address is not incremented for each transfer.
						        1: The destination address is incremented by the amount specified by Width for each transfer.
						        2: The destination address is incremented by 2 times the amount specified by Width for each transfer.
						        3: The destination address is incremented by 4 times the amount specified by Width for each transfer.
						    - bit6~bit7: reserved. */
	uint8_t  reserved0;
	uint16_t data_length;	///! 0: 1 transfer, 1: 2 transfer, ..., 1023: 1024 transfer. Others: reserved.
	uint16_t reserved1;
	uint32_t src;			///! Source data end address
	uint32_t dst;			///! Destination end address
	uint32_t task_addr;		/**< the address of RAM for saving this task.
							   (NOTE: each task need 16 bytes RAM for storing configuration,
							   and DMA API could set it according user input parameter,
							   but it is responsible of user to allocate this RAM space and
							   make sure that the base address must be 16-byte alignment.
							   And if user has setup the next_task(!=0), the dma_task_link
							   must be called for this task setup, otherwise unpredictable error will happen.) */
} DMA_TASK_T;

/**
 * @brief DMA ROM API structure
 * The DMA API handles DMA set-up and transfers.
 */
typedef struct DMAD_API {
	/** DMA ISR routine */
	void (*dma_isr)(DMA_HANDLE_T *handle);
	/** Get memory size needed for DMA. */
	uint32_t (*dma_get_mem_size)(void);
	/** Set up DMA. */
	DMA_HANDLE_T * (*dma_setup)(uint32_t base_addr, uint8_t * ram);
	/** Enable DMA channel and set-up basic DMA transfer. */
	ErrorCode_t (*dma_init)(DMA_HANDLE_T *handle, DMA_CHANNEL_T *channel, DMA_TASK_T *task);
	/** Create linked transfer. */
	ErrorCode_t (*dma_link)(DMA_HANDLE_T *handle, DMA_TASK_T *task, uint8_t valid);
	/** Set a task to valid. */
	ErrorCode_t (*dma_set_valid)(DMA_HANDLE_T *handle, uint8_t chl_num);
	/** Pause DMA transfer on a given channel. */
	ErrorCode_t (*dma_pause)(DMA_HANDLE_T *handle, uint8_t chl_num);
	/** Resume DMA transfer. */
	ErrorCode_t (*dma_unpause)(DMA_HANDLE_T *handle, uint8_t chl_num);
	/** Cancel DMA transfer on a given channel.*/
	ErrorCode_t (*dma_abort)(DMA_HANDLE_T *handle, uint8_t chl_num);
} DMAD_API_T;

/**
 * @}
 */

/** @defgroup SPIROM_15XX CHIP: LPC15xx SPI ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/**
 * @brief SPI handle type
 * The handle to the instance of the SPI driver. Each SPI has one handle, so there can be
 * several handles for each SPI block. This handle is created by Init API and used by the
 * transfer functions for the corresponding SPI block.
 */
typedef void *SPI_HANDLE_T;

/**
 * @brief SPI DMA configuration structure
 */
typedef struct {
	uint32_t dma_txd_num;	///! DMA channel number in case DMA mode is enabled.
	uint32_t dma_rxd_num;	///! In order to do a SPI RX DMA, a SPI TX DMA is also needed to generated clock.
	DMA_HANDLE_T hDMA;		///! DMA handle
} SPI_DMA_CFG_T;

/**
 * @brief SPI interrupt callback function type
 * @param	err_code: SPI error code
 * @param	n: In interrupt mode, this parameter indicates the number of SPI frames.
 *			In DMA mode, this parameter is always zero.
 */
typedef void (*SPI_CALLBK_T)(ErrorCode_t err_code, uint32_t n);

/**
 * @brief SPI DMA setup callback function type.
 * To set up the DMA channel, the source address, destination address, DMA transfer
 * length, DMA request information must be retrieved from the driver structure which has
 * been originally passed from the ROM_SPI_PARAM_T structure.
 * @param	handle: SPI driver handle
 * @param	dma_cfg: DMA configuration.
 */
typedef ErrorCode_t (*SPI_DMA_REQ_T)(SPI_HANDLE_T handle, SPI_DMA_CFG_T *dma_cfg);

/**
 * @brief SPI configuration structure
 */
typedef struct {
	uint32_t delay;		/**< Configures the delay between SSEL and data transfers and between frames. The
						    value is the content of the SPI DLY register. */
	uint32_t divider;	///! Clock divider value DIVVAL in the SPI DIV register.
	uint32_t config;	/**< Enable SPI, configure master/slave, configure signal phase and polarity. The
						    value is the content of the SPI CFG register. */
	uint32_t error_en;	///! Enables the receive overrun and transmit underrun error interrupts.
} SPI_CONFIG_T;

/**
 * @brief SPI configuration parameter structure
 */
typedef struct {
	uint16_t *tx_buffer;	///! Tx buffer
	uint16_t *rx_buffer;	///! Rx buffer
	uint32_t size;				/**< size of the SPI transfer. A transfer can consist of several transmits of the
								                TXCTLDAT register and of several frames. */
	uint32_t fsize_sel;		/**< write the contents of the SPI TXCTL register to select the data length and the
							                    slave select lines. In slave mode, you need to only select the data length. */
	uint32_t eof_flag;		///! EOF flag. EOF( end of frame ) is needed if set. EOF delay will not be asserted without this flag.
	uint32_t tx_rx_flag;	///! Tx & Rx mode flag. 0 is TX only, 1 is RX only, 0x2 is TX and RX
	uint32_t driver_mode;	/**< Driver mode.
							                    - 0x00: Polling mode. Function is blocked until transfer is finished.
							                    - 0x01: Interrupt mode. Function exits immediately and a call back function is invoked when the transfer has finished
							                    - 0x02: DMA mode. Data transferred by SPI is processed by DMA.
							            The DMA_req function is called foe SPI DMA channel set up.
							            The callback function indicates when the transfer is complete. */
	SPI_DMA_CFG_T *dma_cfg;	///! DMA configuration
	SPI_CALLBK_T cb;		///! SPI interrupt callback function
	SPI_DMA_REQ_T dma_cb;	///! SPI DMA channel set-up call back function.
} SPI_PARAM_T;

/**
 * @brief SPI ROM API structure
 * The SPI API handles SPI data transfer in master and slave modes.
 */
typedef struct {
	/** Memory size for one SPI instance */
	uint32_t (*spi_get_mem_size)(void);
	/** Set up SPI instance and return handle*/
	SPI_HANDLE_T (*spi_setup)(uint32_t base_addr, uint8_t *ram);
	/** Set up SPI operating mode */
	void (*spi_init)(SPI_HANDLE_T handle, SPI_CONFIG_T *set);
	/** Send or receive data in master mode*/
	uint32_t (*spi_master_transfer)(SPI_HANDLE_T handle, SPI_PARAM_T *param);
	/** Send or receive data in slave mode*/
	uint32_t (*spi_slave_transfer)(SPI_HANDLE_T handle, SPI_PARAM_T *param);
	/** Interrupt service routine */
	void (*spi_isr)(SPI_HANDLE_T handle);
} SPID_API_T;

/**
 * @}
 */


/** @defgroup ADCROM_15XX CHIP: LPC15xx ADC ROM API declarations and functions
 * @ingroup ROMAPI_15XX
 * @{
 */

/**
 * @brief ADC handle type
 */
typedef void *ADC_HANDLE_T;

typedef void (*ADC_SEQ_CALLBK_T)(ADC_HANDLE_T handle);
typedef void (*ADC_CALLBK_T)(ErrorCode_t error_code, uint32_t num_channel);

/* Typedef for structure pointed by the ADC_HANDLE */
typedef struct   {	/* block of RAM allocated by the application program */
	uint32_t          base_addr;	/* adcr register base address */
	uint32_t          *seqa_buffer;	/* adc buffer */
	uint32_t          *seqb_buffer;	/* adc buffer */
	uint32_t          seqa_channel_num;	/* number of ADC channels */
	uint32_t          seqb_channel_num;	/* number of ADC channels */
	uint32_t          seqa_hwtrig;
	uint32_t          seqb_hwtrig;
	uint32_t          comp_flags;
	uint32_t          overrun_flags;
	uint32_t          thcmp_flags;
	uint32_t          error_code;	/* error code */
	ADC_SEQ_CALLBK_T  seqa_callback;	/* For interrupt, it's the end of the sequence A */
	ADC_SEQ_CALLBK_T  seqb_callback;	/* For interrupt, it's the end of the sequence B */
	ADC_CALLBK_T      overrun_callback;	/* For interrupt, it's the overrun */
	ADC_CALLBK_T      thcmp_callback;	/* For interrupt, it's over the threshold */
	uint32_t          error_en;	/* enable bits for error detection */
	uint32_t          thcmp_en;	/* enable bits for thcmp detection */
} ADC_DRIVER_T;	/* HEADER_TypeDef	 *********************************/

typedef struct {
	uint32_t system_clock;	/* System clock */
	uint32_t adc_clock;	/* ADC clock */
	uint32_t async_mode;
	uint32_t tenbit_mode;
	uint32_t lpwr_mode;
	uint32_t input_sel;
	uint32_t seqa_ctrl;
	uint32_t seqb_ctrl;
	uint32_t thrsel;
	uint32_t thr0_low;
	uint32_t thr0_high;
	uint32_t thr1_low;
	uint32_t thr1_high;
	uint32_t error_en;
	uint32_t thcmp_en;
	uint32_t channel_num;
} ADC_CONFIG_T;

typedef struct {
	uint32_t dma_adc_num;	/* DMA channel used for ADC data peripheral to memory transfer */
	uint32_t dma_pinmux_num; /* H/W trigger number. */
	uint32_t dma_handle; /* DMA handle passed to ADC */
	ADC_CALLBK_T dma_done_callback_pt;	/* DMA completion callback function */
} ADC_DMA_CFG_T;

typedef ErrorCode_t (*ADC_DMA_SETUP_T)(ADC_HANDLE_T handle, ADC_DMA_CFG_T *dma_cfg);

typedef struct {		/* params passed to adc driver function */
	uint32_t          *buffer;		/* Considering supporting DMA and non-DMA mode, 32-bit buffer is needed for DMA */
	uint32_t          driver_mode;	/* 0x00: Polling mode, function is blocked until transfer is finished. */
									/* 0x01: Interrupt mode, function exit immediately, callback function is invoked when transfer is finished. */
									/* 0x02: DMA mode, in case DMA block is available, data transferred by ADC is processed by DMA, 
									         and max buffer size is the total number ADC channels, DMA req function is called for ADC DMA
									         channel setup, then SEQx completion also used as DMA callback function when that ADC conversion/DMA transfer
									         is finished. */
	uint32_t          seqa_hwtrig;	/* H/W trigger for sequence A */
	uint32_t          seqb_hwtrig;	/* H/W trigger for sequence B */
	ADC_CONFIG_T      *adc_cfg;
	uint32_t          comp_flags;
	uint32_t          overrun_flags;
	uint32_t          thcmp_flags;
	ADC_DMA_CFG_T     *dma_cfg;
	ADC_SEQ_CALLBK_T  seqa_callback_pt;		/* SEQA callback function/the same callback on DMA completion if DMA is used for ADCx. */
	ADC_SEQ_CALLBK_T  seqb_callback_pt;		/* SEQb callback function/the same callback on DMA completion if DMA is used for ADCx. */
	ADC_CALLBK_T      overrun_callback_pt;	/* Overrun callback function */
	ADC_CALLBK_T      thcmp_callback_pt;	/* THCMP callback function */
	ADC_DMA_SETUP_T   dma_setup_func_pt;	/* ADC DMA channel setup function */
} ADC_PARAM_T;

/* Typedef Structure for ROM APIs */
typedef struct ADCD_API {
	/* ADC Configuration functions */
	uint32_t (*adc_get_mem_size)(void);
	ADC_HANDLE_T (*adc_setup)(uint32_t base_addr, uint8_t *ram);
	void (*adc_calibration)(ADC_HANDLE_T handle, ADC_CONFIG_T *set);
	void (*adc_init)(ADC_HANDLE_T handle, ADC_CONFIG_T *set);
	
	/* ADC Conversion Functions */
	uint32_t (*adc_seqa_read)(ADC_HANDLE_T handle, ADC_PARAM_T *param);
	uint32_t (*adc_seqb_read)(ADC_HANDLE_T handle, ADC_PARAM_T *param);
	
	/* ADC Interrupt handlers */
	void (*adc_seqa_isr)(ADC_HANDLE_T handle);
	void (*adc_seqb_isr)(ADC_HANDLE_T handle);
	void (*adc_ovr_isr)(ADC_HANDLE_T handle);
	void (*adc_thcmp_isr)(ADC_HANDLE_T handle);
	
	uint32_t  (*adc_get_firmware_version)(void);
} ADCD_API_T;

/**
 * @}
 */

 
#ifdef __cplusplus
}
#endif

#endif /* __ROMAPI_15XX_H_ */
