/** NVM library
	Non volatile memory implemented via an I2C EEPROM
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2012 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#include "global.h"
#include "sys.h"
#include "i2c.h"
#include "nvmi2c.h"
#include "prc.h"
#include "systime.h"

/** NVRAM initialization - currently an empty placeholder */
void NVMI2C_Init(void) {
	// dummy
}

/** Wait for the current NVRAM operation to be completed (poll I2C EEPROM acknowledge) */
void NVMI2C_WaitForCompletion(void) {
	u8 queue_ready;
	// wait until the EEPROM answers to acknowledge polling
	do {
		do {
			PRC_Int_Disable();
			{
				queue_ready = I2C_QueueEntriesAvailable(I2C_CH_EEPROM_ACK)>0;
				if (queue_ready)
					I2C_MasterTransfer(I2C_CH_EEPROM_ACK, I2C_TYPE_MST_READ, 0, NULL, 0, NULL);
			}
			PRC_Int_Enable();
		} while (!queue_ready);
		SYSTIME_WaitUs(1000);
		// wait until the communication has finished
		while(I2C_GetChannelPendingCount(I2C_CH_EEPROM_ACK)>0);
	} while (I2C_GetFaults(I2C_CH_EEPROM_ACK)!= 0);
}

/** Write data at a given address.
	Use page writes and minimize number of communications.
	@param adr    start address  in NVRAM
	@param data   pointer to buffer which contains data to be written
	@param len    number of bytes to be written
	@return success
 */
u8 NVMI2C_Write(u16 adr, u8 *data, u16 len) {
	u8 success=0;
	u8 queue_ready;
	u16 l;
	u16 cnt=0;

	// check how many bytes will fit into this page
	l = NVM_I2C_PAGE_SIZE - (adr % NVM_I2C_PAGE_SIZE);
	if (len < l)
		l = len;
	do {
		do {
			PRC_Int_Disable();
			{
				queue_ready = I2C_QueueEntriesAvailable(I2C_CH_EEPROM)>0;
				if (queue_ready)
					success = I2C_MasterTransfer(I2C_CH_EEPROM, I2C_TYPE_MST_WRITE, adr, data, l, NULL);
			}
			PRC_Int_Enable();
		} while (!queue_ready);

		cnt  += l;
		adr  += l;
		data += l;
		// calculate how many bytes are left
		l = len - cnt;
		if (l > NVM_I2C_PAGE_SIZE)
			l = NVM_I2C_PAGE_SIZE;
		if (l>0)
			NVMI2C_WaitForCompletion();
	} while ((cnt < len) && (success != 0));

	return success;
}

/** Read data from a given address
	@param adr    start address in NVRAM
	@param data   pointer to buffer where read data is stored
	@param len    number of bytes to be read
	@return success
 */
u8 NVMI2C_Read(u16 adr, u8 *data, u16 len) {
	return I2C_MasterTransfer(I2C_CH_EEPROM, I2C_TYPE_MST_READ, adr, data, len, NULL);
}

// Sequential read is already used in NVM_Read to transfer more than one byte
// Calling it explicitly after a normal Read operation which ended with STOP
// obviously doesn't work.
// For this we had to implement a Read command that doesn't send a STOP after
// the last byte was read.

