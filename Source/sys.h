/** System configuration
	Configuration of system resources (CPU clock, prescalers etc.)
	LPC15xx ARM Cortex M3
	----------------------------------------------------------
	Copyright 2015 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SYS_H
#define SYS_H

#include "sys_cnf.h"

/** Internal oscillator frequency */
#define SYSCTL_IRC_FREQ    12000000UL

/** Internal watchdog oscillator frequency */
#define SYSCTL_WDTOSC_FREQ 503000UL

/** Real time clock frequency */
#define SYSCTL_RTCOSC_FREQ 32768U

/* System clock register 0 */
#define SYS_CLK_SYS        (1<<0)
#define SYS_CLK_ROM        (1<<1)
#define SYS_CLK_SRAM1      (1<<3)
#define SYS_CLK_SRAM2      (1<<4)
#define SYS_CLK_FLASH      (1<<7)
#define SYS_CLK_EEPROM     (1<<9)
#define SYS_CLK_MUX        (1<<11)
#define SYS_CLK_SWM        (1<<12)
#define SYS_CLK_IOCON      (1<<13)
#define SYS_CLK_GPIO0      (1<<14)
#define SYS_CLK_GPIO1      (1<<15)
#define SYS_CLK_GPIO2      (1<<16)
#define SYS_CLK_PINT       (1<<18)
#define SYS_CLK_GINT       (1<<19)
#define SYS_CLK_DMA        (1<<20)
#define SYS_CLK_CRC        (1<<21)
#define SYS_CLK_WWDT       (1<<22)
#define SYS_CLK_RTC        (1<<23)
#define SYS_CLK_ADC0       (1<<27)
#define SYS_CLK_ADC1       (1<<28)
#define SYS_CLK_DAC        (1<<29)
#define SYS_CLK_ACMP       (1<<30)

/* System clock register 1 */
#define SYS_CLK_MRT        (1<<0)
#define SYS_CLK_RIT        (1<<1)
#define SYS_CLK_SCT0       (1<<2)
#define SYS_CLK_SCT1       (1<<3)
#define SYS_CLK_SCT2       (1<<4)
#define SYS_CLK_SCT3       (1<<5)
#define SYS_CLK_SCTIPU     (1<<6)
#define SYS_CLK_CCAN       (1<<7)
#define SYS_CLK_SPI0       (1<<9)
#define SYS_CLK_SPI1       (1<<10)
#define SYS_CLK_I2C0       (1<<13)
#define SYS_CLK_UART0      (1<<17)
#define SYS_CLK_UART1      (1<<18)
#define SYS_CLK_UART2      (1<<19)
#define SYS_CLK_QEI        (1<<21)
#define SYS_CLK_USB        (1<<23)

/* System clock register 0 */
#define SYS_CLK_SYS_CFG(x)     (((x)&1)<<0)
#define SYS_CLK_ROM_CFG(x)     (((x)&1)<<1)
#define SYS_CLK_SRAM1_CFG(x)   (((x)&1)<<3)
#define SYS_CLK_SRAM2_CFG(x)   (((x)&1)<<4)
#define SYS_CLK_FLASH_CFG(x)   (((x)&1)<<7)
#define SYS_CLK_EEPROM_CFG(x)  (((x)&1)<<9)
#define SYS_CLK_MUX_CFG(x)     (((x)&1)<<11)
#define SYS_CLK_SWM_CFG(x)     (((x)&1)<<12)
#define SYS_CLK_IOCON_CFG(x)   (((x)&1)<<13)
#define SYS_CLK_GPIO0_CFG(x)   (((x)&1)<<14)
#define SYS_CLK_GPIO1_CFG(x)   (((x)&1)<<15)
#define SYS_CLK_GPIO2_CFG(x)   (((x)&1)<<16)
#define SYS_CLK_PINT_CFG(x)    (((x)&1)<<18)
#define SYS_CLK_GINT_CFG(x)    (((x)&1)<<19)
#define SYS_CLK_DMA_CFG(x)     (((x)&1)<<20)
#define SYS_CLK_CRC_CFG(x)     (((x)&1)<<21)
#define SYS_CLK_WWDT_CFG(x)    (((x)&1)<<22)
#define SYS_CLK_RTC_CFG(x)     (((x)&1)<<23)
#define SYS_CLK_ADC0_CFG(x)    (((x)&1)<<27)
#define SYS_CLK_ADC1_CFG(x)    (((x)&1)<<28)
#define SYS_CLK_DAC_CFG(x)     (((x)&1)<<29)
#define SYS_CLK_ACMP_CFG(x)    (((x)&1)<<30)

/* System clock register 1 */
#define SYS_CLK_MRT_CFG(x)     (((x)&1)<<0)
#define SYS_CLK_RIT_CFG(x)     (((x)&1)<<1)
#define SYS_CLK_SCT0_CFG(x)    (((x)&1)<<2)
#define SYS_CLK_SCT1_CFG(x)    (((x)&1)<<3)
#define SYS_CLK_SCT2_CFG(x)    (((x)&1)<<4)
#define SYS_CLK_SCT3_CFG(x)    (((x)&1)<<5)
#define SYS_CLK_SCTIPU_CFG(x)  (((x)&1)<<6)
#define SYS_CLK_CCAN_CFG(x)    (((x)&1)<<7)
#define SYS_CLK_SPI0_CFG(x)    (((x)&1)<<9)
#define SYS_CLK_SPI1_CFG(x)    (((x)&1)<<10)
#define SYS_CLK_I2C0_CFG(x)    (((x)&1)<<13)
#define SYS_CLK_UART0_CFG(x)   (((x)&1)<<17)
#define SYS_CLK_UART1_CFG(x)   (((x)&1)<<18)
#define SYS_CLK_UART2_CFG(x)   (((x)&1)<<19)
#define SYS_CLK_QEI_CFG(x)     (((x)&1)<<21)
#define SYS_CLK_USB_CFG(x)     (((x)&1)<<23)

/* Power configuration register */
#define SYSCTL_POWERDOWN_IRCOUT_PD  (1 << 3)	///! IRC oscillator output power-down
#define SYSCTL_POWERDOWN_IRC        (1 << 4)	///! IRC oscillator power-down
#define SYSCTL_POWERDOWN_FLASH      (1 << 5)	///! Flash memory power-down
#define SYSCTL_POWERDOWN_EEPROM     (1 << 6)	///! EEPROM power-down
#define SYSCTL_POWERDOWN_BOD_PD     (1 << 8)	///! BOD power-down
#define SYSCTL_POWERDOWN_USBPHY_PD  (1 << 9)	///! USB PHY power-down
#define SYSCTL_POWERDOWN_ADC0_PD    (1 << 10)	///! ADC0 power-down
#define SYSCTL_POWERDOWN_ADC1_PD    (1 << 11)	///! ADC1 power-down
#define SYSCTL_POWERDOWN_DAC_PD     (1 << 12)	///! DAC power-down
#define SYSCTL_POWERDOWN_ACMP0_PD   (1 << 13)	///! ACMP0 power-down
#define SYSCTL_POWERDOWN_ACMP1_PD   (1 << 14)	///! ACMP1 power-down
#define SYSCTL_POWERDOWN_ACMP2_PD   (1 << 15)	///! ACMP2 power-down
#define SYSCTL_POWERDOWN_ACMP3_PD   (1 << 16)	///! ACMP3 power-down
#define SYSCTL_POWERDOWN_IREF_PD    (1 << 17)	///! Internal voltage reference power-down
#define SYSCTL_POWERDOWN_TS_PD      (1 << 18)	///! Temperature sensor power-down
#define SYSCTL_POWERDOWN_VDDADIV_PD (1 << 19)	///! VDDA divider power-down
#define SYSCTL_POWERDOWN_WDTOSC_PD  (1 << 20)	///! Watchdog oscillator power-down
#define SYSCTL_POWERDOWN_SYSOSC_PD  (1 << 21)	///! System oscillator power-down
#define SYSCTL_POWERDOWN_SYSPLL_PD  (1 << 22)	///! System PLL power-down
#define SYSCTL_POWERDOWN_USBPLL_PD  (1 << 23)	///! USB PLL power-down
#define SYSCTL_POWERDOWN_SCTPLL_PD  (1 << 24)	///! SCT PLL power-down

#define SYSCTL_PowerUp(x)               {LPC_SYSCTL->PDRUNCFG &= (uint32_t)~(x);}
#define SYSCTL_PowerDown(x)             {LPC_SYSCTL->PDRUNCFG |= (uint32_t)(x);}

#define SYSCTL_PLLCLKSRC_IRC         0			///! Internal oscillator in (may not work for USB)
#define SYSCTL_PLLCLKSRC_SYSOSC      1			///! Crystal Oscillator (SYSOSC)

#define SYSCTL_MAINCLKSELA_IRC       0			///! Internal RC oscillator
#define SYSCTL_MAINCLKSELA_SYSOSC    1			///! System oscillator
#define SYSCTL_MAINCLKSELA_WDOSC     2			///! Watchdog oscillator

#define SYSCTL_MAINCLKSELB_PLL_IN    3          ///! 1: PLL input
#define SYSCTL_MAINCLKSELB_PLL_OUT   4          ///! 2: PLL output
#define SYSCTL_MAINCLKSELB_RTC       5          ///! 3: Real time clock 32kHz

#define SYSCTL_USBCLKSEL_IRC         0          ///! Internal RC oscillator
#define SYSCTL_USBCLKSEL_SYSOSC      1          ///! System oscillator
#define SYSCTL_USBCLKSEL_USBPLL      2          ///! USB PLL output
#define SYSCTL_USBCLKSEL_MAIN        3          ///! Main Clock

#define SYSCTL_ADCASYNCCLKSEL_IRC    0          ///! Internal RC oscillator
#define SYSCTL_ADCASYNCCLKSEL_SYSOSC 1          ///! System oscillator
#define SYSCTL_ADCASYNCCLKSEL_USBPLL 2          ///! USB PLL output
#define SYSCTL_ADCASYNCCLKSEL_SCTPLL 3          ///! SCT PLL output

#define SYSCTL_CLKOUTSELA_IRC        0          ///! Internal RC oscillator
#define SYSCTL_CLKOUTSELA_SYSOSC     1          ///! System oscillator
#define SYSCTL_CLKOUTSELA_WDOSC      2          ///! USB PLL output
#define SYSCTL_CLKOUTSELA_MAIN       3          ///! Main Clock

#define SYSCTL_CLKOUTSELB_USBPLL     4          ///! 1: USB PLL output
#define SYSCTL_CLKOUTSELB_SCTPLL     5          ///! 2: SCT PLL output
#define SYSCTL_CLKOUTSELB_RTC        6          ///! 3: Real time clock 32kHz


#define SYSCTL_SYSPLL_SetSource(src)    {LPC_SYSCTL->SYSPLLCLKSEL  = (uint32_t)(src);}
#define SYSCTL_SYSPLL_Setup(msel, psel) {LPC_SYSCTL->SYSPLLCTRL = ((msel) & 0x3F) | (((psel) & 0x3) << 6);}
#define SYSCTL_SYSPLL_Locked()          ((LPC_SYSCTL->SYSPLLSTAT & 1) != 0)

#define SYSCTL_USBPLL_SetSource(src)    {LPC_SYSCTL->USBPLLCLKSEL  = (uint32_t)(src);}
#define SYSCTL_USBPLL_Setup(msel, psel) {LPC_SYSCTL->USBPLLCTRL = ((msel) & 0x3F) | (((psel) & 0x3) << 6);}
#define SYSCTL_USBPLL_Locked()          ((LPC_SYSCTL->USBPLLSTAT & 1) != 0)

#define SYSCTL_SCTPLL_SetSource(src)    {LPC_SYSCTL->SCTPLLCLKSEL  = (uint32_t)(src);}
#define SYSCTL_SCTPLL_Setup(msel, psel) {LPC_SYSCTL->SCTPLLCTRL = ((msel) & 0x3F) | (((psel) & 0x3) << 6);}
#define SYSCTL_SCTPLL_Locked()          ((LPC_SYSCTL->SCTPLLSTAT & 1) != 0)

#define SYSCTL_SetSysClockDiv(div)      { LPC_SYSCTL->SYSAHBCLKDIV  = (div); }

#define SYSCTL_GetMsel(reg) (((reg) & 0x3F)+1)
#define SYSCTL_GetPsel(reg) (((((reg)>>6)&3) == 3) ? 8 : ((((reg)>>6)&3)/2))

/* PLL setup */
#if SYS_MSEL_CFG<1 || SYS_MSEL_CFG>64
#error "MSEL value for PLL setup out of range"
#endif

#if !(SYS_PSEL_CFG==1 || SYS_PSEL_CFG==2 || SYS_PSEL_CFG==4 || SYS_PSEL1_CFG==8)
#error "PSEL value for SYS PLL setup out of range"
#endif

#if SYS_PSEL_CFG==8
#define SYS_PSEL_CFG_ 3
#else
#define SYS_PSEL_CFG_ (SYS_PSEL_CFG/2)
#endif

#define SYSPLLCFG_CFG (((SYS_MSEL_CFG - 1)&0x3f) | (((SYS_PSEL_CFG_)&3)<<6))


/* USB PLL setup */
#if SYS_MSEL_USB_CFG<1 || SYS_MSEL_USB_CFG>64
#error "MSEL value for USB PLL setup out of range"
#endif

#if !(SYS_PSEL_USB_CFG==1 || SYS_PSEL_USB_CFG==2 || SYS_PSEL_USB_CFG==4 || SYS_PSEL_USB_CFG==8)
#error "PSEL value for USB PLL setup out of range"
#endif

#if SYS_PSEL_USB_CFG==8
#define SYS_PSEL_USB_CFG_ 3
#else
#define SYS_PSEL_USB_CFG_ (SYS_PSEL_USB_CFG/2)
#endif

#define USBPLLCFG_CFG (((SYS_MSEL_USB_CFG - 1)&0x3f) | (((SYS_PSEL_USB_CFG_)&3)<<6))


/* SCT PLL setup */
#if SYS_MSEL_SCT_CFG<1 || SYS_MSEL_SCT_CFG>64
#error "MSEL value for SCT PLL setup out of range"
#endif

#if !(SYS_PSEL_SCT_CFG==1 || SYS_PSEL_SCT_CFG==2 || SYS_PSEL_SCT_CFG==4 || SYS_PSEL_SCT_CFG==8)
#error "PSEL value for SCT PLL setup out of range"
#endif

#if SYS_PSEL_SCT_CFG==8
#define SYS_PSEL_SCT_CFG_ 3
#else
#define SYS_PSEL_SCT_CFG_ (SYS_PSEL_SCT_CFG/2)
#endif

#define SCTPLLCFG_CFG (((SYS_MSEL_SCT_CFG - 1)&0x3f) | (((SYS_PSEL_SCT_CFG_)&3)<<6))


/* check frequency setup */
#if SYS_PLL_CLKSRC_CFG  == SYSCTL_PLLCLKSRC_SYSOSC
#define SYS_FRQ_PLLIN SYS_FRQ_OSC
#else
#define SYS_FRQ_PLLIN SYSCTL_IRC_FREQ
#endif

#define SYS_FRQ_PLL (SYS_FRQ_PLLIN*SYS_MSEL_CFG)

#if SYS_FRQ_PLL > 100000000UL
#error "Maximum system PLL output frequency exceeded"
#endif

#if (SYS_FRQ_PLL*2*SYS_PSEL_CFG < 156000000UL) || (SYS_FRQ_PLL*2*SYS_PSEL_CFG > 320000000UL)
#error "Limits of FCCO frequency exceeded"
#endif

#if SYS_USBPLL_CLKSRC_CFG  == SYSCTL_PLLCLKSRC_SYSOSC
#define SYS_FRQ_USBPLLIN SYS_FRQ_OSC
#else
#define SYS_FRQ_USBPLLIN SYSCTL_IRC_FREQ
#endif

#define SYS_FRQ_USBPLL (SYS_FRQ_USBPLLIN*SYS_MSEL_USB_CFG)

#if SYS_FRQ_USBPLL > 100000000UL
#error "Maximum USB PLL output frequency exceeded"
#endif

#if (SYS_FRQ_USBPLL*2*SYS_PSEL_USB_CFG < 156000000UL) || (SYS_FRQ_USBPLL*2*SYS_PSEL_USB_CFG > 320000000UL)
#error "Limits of USB FCCO frequency exceeded"
#endif

#if SYS_SCTPLL_CLKSRC_CFG  == SYSCTL_PLLCLKSRC_SYSOSC
#define SYS_FRQ_SCTPLLIN SYS_FRQ_OSC
#else
#define SYS_FRQ_SCTPLLIN SYSCTL_IRC_FREQ
#endif

#define SYS_FRQ_SCTPLL (SYS_FRQ_SCTPLLIN*SYS_MSEL_SCT_CFG)

#if SYS_FRQ_SCTPLL > 100000000UL
#error "Maximum SCT PLL output frequency exceeded"
#endif

#if (SYS_FRQ_SCTPLL*2*SYS_PSEL_SCT_CFG < 156000000UL) || (SYS_FRQ_SCTPLL*2*SYS_PSEL_SCT_CFG > 320000000UL)
#error "Limits of SCT FCCO frequency exceeded"
#endif


/* Main clock */
#if SYS_MAIN_CLKSEL_CFG == SYSCTL_MAINCLKSELB_RTC
#define SYS_FRQ_MAIN SYSCTL_RTCOSC_FREQ
#elif SYS_MAIN_CLKSEL_CFG == SYSCTL_MAINCLKSELA_WDOSC
#define SYS_FRQ_MAIN SYSCTL_WDTOSC_FREQ
#elif SYS_MAIN_CLKSEL_CFG == SYSCTL_MAINCLKSELA_IRC
#define SYS_FRQ_MAIN SYSCTL_IRC_FREQ
#elif  SYS_MAIN_CLKSEL_CFG == SYSCTL_MAINCLKSELA_SYSOSC
#define SYS_FRQ_MAIN SYS_FRQ_OSC
#elif SYS_MAIN_CLKSEL_CFG == SYSCTL_MAINCLKSELB_PLL_IN
#define SYS_FRQ_MAIN SYS_FRQ_PLLIN
#else
#define SYS_FRQ_MAIN SYS_FRQ_PLL
#endif

#if (SYS_FRQ_MAIN/SYS_CLK_DIV) != SYS_FRQ_CORE
#error "Clock setup doesn't fit expected value"
#endif

/* USB Clock */
#if SYS_CLK_USB_CLKSEL_CFG == SYSCTL_USBCLKSEL_IRC
#define SYS_FRQ_USB_BASE SYSCTL_IRC_FREQ
#elif SYS_CLK_USB_CLKSEL_CFG == SYSCTL_USBCLKSEL_SYSOSC
#define SYS_FRQ_USB_BASE SYS_FRQ_OSC
#elif SYS_CLK_USB_CLKSEL_CFG == SYSCTL_USBCLKSEL_USBPLL
#define SYS_FRQ_USB_BASE SYS_FRQ_USBPLL
#elif SYS_CLK_USB_CLKSEL_CFG == SYSCTL_USBCLKSEL_MAIN
#define SYS_FRQ_USB_BASE SYS_FRQ_MAIN
#endif

#if SYS_FRQ_USB_BASE/SYS_CLK_USB_DIV != SYS_FRQ_USB
#error "USB Clock setup doesn't fit expected value"
#endif

/* ADC_AYSNC Clock */
#if SYS_CLK_ADCASYNC_CLKSEL_CFG == SYSCTL_ADCASYNCCLKSEL_IRC
#define SYS_FRQ_ADCASYNC_BASE SYSCTL_IRC_FREQ
#elif SYS_CLK_ADCASYNC_CLKSEL_CFG == SYSCTL_ADCASYNCCLKSEL_SYSOSC
#define SYS_FRQ_ADCASYNC_BASE SYS_FRQ_OSC
#elif SYS_CLK_ADCASYNC_CLKSEL_CFG == SYSCTL_ADCASYNCCLKSEL_USBPLL
#define SYS_FRQ_ADCASYNC_BASE SYS_FRQ_USBPLL
#elif SYS_CLK_ADCASYNC_CLKSEL_CFG == SYSCTL_ADCASYNCCLKSEL_SCTPLL
#define SYS_FRQ_ADCASYNC_BASE SYS_FRQ_SCTPLL
#endif

#if SYS_FRQ_ADCASYNC_BASE/SYS_CLK_ADC_AYSNC_DIV != SYS_FRQ_ASYNC_ADC
#error "ADC Clock setup doesn't fit expected value"
#endif

#if SYS_FRQ_USB_BASE/SYS_CLK_USB_DIV != SYS_FRQ_USB
#error "USB Clock setup doesn't fit expected value"
#endif

/* Clockout */
#if SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCTL_CLKOUTSELB_RTC
#define SYS_FRQ_CLKOUT_BASE SYSCTL_RTCOSC_FREQ
#elif SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCTL_CLKOUTSELA_WDOSC
#define SYS_FRQ_CLKOUT_BASE SYSCTL_WDTOSC_FREQ
#elif SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCTL_CLKOUTSELA_IRC
#define SYS_FRQ_CLKOUT_BASE SYSCTL_IRC_FREQ
#elif  SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCTL_CLKOUTSELA_SYSOSC
#define SYS_FRQ_CLKOUT_BASE SYS_FRQ_OSC
#elif SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCTL_CLKOUTSELB_USBPLL
#define SYS_FRQ_CLKOUT_BASE SYS_FRQ_USBPLL
#else
#define SYS_FRQ_CLKOUT_BASE SYS_FRQ_SCTPLL
#endif

#if SYS_FRQ_SCT != SYS_FRQ_SCTPLL
#error "SCT clock setup doesn't fit expected value"
#endif

#define SYS_FRQ               SYS_FRQ_CORE

/* Peripheral clocks */
#define SYS_CLK_SYSTICK_FRQ   (SYS_FRQ/SYS_CLK_SYSTICK_DIV)
#define SYS_CLK_UART_FRQ      (SYS_FRQ/SYS_CLK_UART_DIV)
#define SYS_CLK_IOCON_FRQ     (SYS_FRQ/SYS_CLK_IOCON_DIV)
#define SYS_CLK_TRACE_FRQ     (SYS_FRQ/SYS_CLK_TRACE_DIV)

#define SYS_CLK_SPI_FRQ       SYS_FRQ
#define SYS_CLK_I2C_FRQ       SYS_FRQ

/* Peripheral clocks with own clock selection */
/* @todo: consider clock selection configuration */
#define SYS_CLK_USB_FRQ       (SYS_FRQ_USB_BASE/SYS_CLK_USB_DIV)
#define SYS_CLK_ADC_AYSNC_FRQ (SYS_FRQ_ADCASYNC_BASE/SYS_CLK_ADC_AYSNC_DIV)
#define SYS_CLK_CLKOUT_FRQ    (SYS_FRQ_CLKOUT_BASE/SYS_CLK_CLKOUT_DIV)

#endif