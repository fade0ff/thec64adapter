DSUB9 to USB adapter for "The C64"

http://www.lemmini.de/TheC64Adapter/TheC64Adapter.html
---------------------------------------------------------------
Copyright 2021 Volker Oth - VolkerOth(at)gmx.de

Licensed under the Creative Commons Attribution 4.0 license
http://creativecommons.org/licenses/by/4.0/

Everything in this repository is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OF ANY KIND, either express or implied.